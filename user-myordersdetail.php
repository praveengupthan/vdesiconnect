<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Order History </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">My order History</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li>                              
                                <li><a href="user-myorders.php">Order History </a></li>
                                <li><a>Order Name will be here </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">My order History <a class="fgreen float-right bankclink" href="user-myorders.php"><span class="icon-left-arrow icomoon"></span>Back to my Orders</a></h5>
                                <!-- gray block -->
                                <div class="grayblock">
                                    <ul class="row primarydetails">
                                        <li class="col-lg-4">
                                            <h6>Name</h6>
                                            <p>Praveen Kumar Nandipati</p>
                                        </li>
                                        <li class="col-lg-4">
                                            <h6>Order number	</h6>
                                            <p>18100614451880850561	</p>
                                        </li>
                                        <li class="col-lg-4">
                                            <h6>Order Date & Time </h6>
                                            <p>06 Oct 2018 14:45:19	</p>
                                        </li>
                                    </ul>
                                    <ul class="row primarydetails">
                                        <li class="col-lg-4">
                                            <h6>Payment Method</h6>
                                            <p>Netbanking/Card/Wallet </p>
                                        </li>
                                        <li class="col-lg-4">
                                            <h6>Total 	<span class=" float-right">Rs: 8315</span>	</h6>
                                            <p class="border-bottom">Delivery Charges :<span class=" float-right">Rs: 230</span>	</p>
                                            <h5 class="h5 py-3">Payble Amount <span class="forange  float-right">Rs:8315</span></h5>
                                        </li>                                       
                                    </ul>
                                </div>
                                <!--/ gray block -->

                                <!-- row -->
                                <div class="row py-4">
                                    <!-- col -->
                                    <div class="col-lg-2 col-md-3">
                                        <figure class="imgproduct">
                                            <a href="javascript:void(0)"><img src="img/data/flowers/flower02.jpg" alt="" title="" class="img-fluid"></a>
                                        </figure>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-10 col-md-9">
                                        <h6 class="pb-2">Product Name will be here</h6>
                                        <p>Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                       
                                        <a href="javascript:void(0)" class="whitebtn">Paynow</a>                                       
                                        <a href="javascript:void(0)" class="whitebtn">return / replace</a>
                                    </div>
                                    <!--/ col -->                                                        
                                </div>
                                <!--/ row -->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6"><h6 class="h6 ">Status: <span class="fgreen">Delivered</span></h6></div>
                                    <div class="col-lg-6 col-md-6 text-right"> <h6 class="h6 ">Delivered on: <span class="fgreen">11 July 2019</span></h6></div>
                                </div>
                                 <!-- order status -->
                                 <div class="ord-status d-flex dashedbrd">
                                    <div class="barcol"> <span class="fgreen sttext">Ordered</span>
                                        <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></a> </div>
                                    </div>
                                    <div class="barcol"> <span class="fgreen sttext">Packed</span>
                                        <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></a> </div>
                                    </div>
                                    <div class="barcol"> <span class="fgreen sttext">Shipped</span> <span class="fgreen sttext deliveredst">Delivered</span>
                                        <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></i></a> 
                                        <a href="javascript:void(0)" class="delivered" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></a> </div>
                                    </div>
                                </div>
                                <!--/ order status -->
                                <p class="py-3">Replacement was allowed till Sat, 25 Oct 2014.</p>

                                <div class="grayblock w-50 p-4 tw100 ">
                                    <p class="fgreen fbold">Shipping Information</p>
                                    <p>Praveen Guptha Nandipati</p>
                                    <p>Plot No:25.1, 8-3-833/25, Phase I, Kamalapuri Colony, Hyderabad ,Near Satyasai Nigamagam , Hyderabad, Telangana - 500072
                                       </p>
                                       <p>Mobile No: 7995165789</p>
                                </div>
                                
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
    <!--/ pupup for write review -->
    <div class="modal fade" id="reviewpopup">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Write Review</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <form class="formreview">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Write Review</label>
                    <textarea class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>Select Rating</label>
                    <select class="form-control">
                        <option>Star 1</option>
                        <option>Star 2</option>
                        <option>Star 3</option>
                        <option>Star 4</option>
                        <option>Star 5</option>
                    </select>
                </div>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success">Submit Review</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>          
        </div>  
        <!-- article -->
        <div class="articlereview p-3">
            <h5 class="h5">How to write customer-friendly reviews?</h5>
            <div>               
                <p>Product centricity: Concentrate only on product feedback and DO NOT include service or seller feedback. Reviews with such content will NOT BE made live
                </p>
                <p>Simple language: Keep the language simple, light and in your own words</p>
                <p>Be crisp: If the details run long, break the review into readable short paragraphs and bullets</p>
                <p>No profanity: Profanity, copyrighted comm
                    
                ents or any copied content must be avoided</p>
                <p>Share your experience: Explain what you liked and disliked about the product. The most helpful reviews are the ones that help readers know exactly what to expect.</p>
                <p>Pros & cons: Select or add appropriate pros & cons to give a quick summary of your review to the reader</p>
            </div>
        </div>
        <!--/ article -->      
      </div>
    </div>
  </div>
  
</div>
    <!--/ popup for write review -->
</body>
</html>