<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Review Order</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Review Order</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="cart.php">My Cart </a></li>                              
                                <li><a>Review Order </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3">
                        <div class="row">                    
                            <div class="col-lg-6">
                                <h4 class="pl-2">Your Cart Items (6)</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <h5 class="pr-2">Rs: 3,000.00</h5>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-9 col-md-7">
                            <!-- row -->
                            <div class="row">
                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-2">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3">
                                                <figure>
                                                    <a href="javascript:void(0)"><img src="img/data/cakes/cake02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 pl-0">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="javascript:void(0)">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>                                                   
                                                </div>                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-2">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3">
                                                <figure>
                                                    <a href="javascript:void(0)"><img src="img/data/cakes/cake03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 pl-0">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="javascript:void(0)">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>                                                    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-2">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3">
                                                <figure>
                                                    <a href="javascript:void(0)"><img src="img/data/cakes/cake04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 pl-0">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="javascript:void(0)">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>                                                   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->
                               
                                <!-- address column -->
                                <div class="col-lg-12 pt-2">
                                    <div class="whitebox p-3">
                                       <div class="row">
                                           <div class="col-lg-8">
                                                <p class="fblue">Delivery Address</p>
                                                <p>Praveen Guptha Nandipati</p>
                                                <p>Plot No:25.1, 8-3-833/25, Phase I, Opp: ICOMM Building, Kamalapuri colony, Srinagar Colony, Hyderabad, Telangana <a href="deliveryaddrerss.php">Change Address</a></p>
                                                <p>Phone: 9642123254</p>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                                <!-- address column -->
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                        <div class="col-lg-3 col-md-5">
                            <div class="rtcart">
                                <h6 class="pb-2">Cart Items (6 Items)</h6>
                                <ul>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>                                    
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>                                        
                                        <p class="pb-0"><span class="fgreen fbold">Total: 	</span> <span class="float-right  fgreen">Rs. 8,880</span></p>
                                        <p class="pb-0"><span>Delivery Charges:  	</span> <span class="float-right">+Rs. 99</span></p>
                                    </li>
                                </ul>
                                <h4 class="fgreen fbold py-2 h4">You Pay <span class="float-right fgreen fbold">Rs. 8,880</span></h4>
                                <a href="makepayment.php" class="greenlink w-100">Proceed to Payment</a>
                                
                            </div>
                        </div>
                        <!--/ right cart -->
                    </div>
                    <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>