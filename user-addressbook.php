<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Change Password </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Address Book</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li>                              
                                <li><a>Address Book</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">Address Book <a href="javascript:void(0)" class="float-right fgreen" data-toggle="modal" data-target="#newaddress">+ Add New Address</a></h5>
                                <!-- row -->
                                <div class="row pt-3">
                                    <!-- col -->
                                    <div class="col-lg-4">
                                        <div class="addresscol whitebox">
                                            <p class="fgreen fbold">Default - Home</p>
                                            <p>Praveen Guptha Nandipati</p>
                                            <p>Plot No:25.1, 8-3-833/25, Phase I, Opp: ICOMM Building, Kamalapuri colony, Srinagar Colony, Hyderabad, Telangana</p>
                                            <p>Phone: 9642123254</p>                                           
                                            <p class="pt-3">
                                                <a href="javascript:void(0)"><span class="icon-edit icomoon"></span>Edit address </a>
                                                <a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Delete </a>
                                            </p>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-4">
                                        <div class="addresscol whitebox">
                                            <p class="fblue fbold">Default - Office</p>
                                            <p>Praveen Guptha Nandipati</p>
                                            <p>Plot No:25.1, 8-3-833/25, Phase I, Opp: ICOMM Building, Kamalapuri colony, Srinagar Colony, Hyderabad, Telangana</p>
                                            <p>Phone: 9642123254</p>                                           
                                            <p class="pt-3">
                                                <a href="javascript:void(0)"><span class="icon-edit icomoon"></span>Edit address </a>
                                                <a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Delete </a>
                                            </p>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    
                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
       
<!--/ Add New Address -->
<div class="modal fade" id="newaddress">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add New Delivery Address</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <form class="formreview">
                 <div class="form-group">
                    <label>Select type of Address</label>
                    <select class="form-control">
                        <option>Office</option>
                        <option>Home</option>
                        <option>Others</option>                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" class="form-control">
                </div>

                <div class="form-group">
                    <label>Area Pincode</label>
                    <input type="text" placeholder="Area Pincode" class="form-control">
                </div>

                <div class="form-group">
                    <label>Address</label>
                    <input type="text" placeholder="H.No/Street Name" class="form-control">
                </div>

                <div class="form-group">
                    <label>Landmark</label>
                    <input type="text" placeholder="Land Mark (Ex: Near Hospital)" class="form-control">
                </div>

                <div class="form-group">
                    <label>Select State</label>
                    <select class="form-control">
                        <option>Telangana</option>
                        <option>Andhra Pradesh</option>
                        <option>Tamilnadu</option>
                        <option>Kerala</option>
                        <option>Maharastra</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select City</label>
                    <select class="form-control">
                        <option>Hyderabad</option>
                        <option>Secunderabad</option>                        
                    </select>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email" class="form-control">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" placeholder="Enter Phone Number" class="form-control">
                </div>
                <div class="form-group">                    
                    <input type="checkbox">
                    <label>Default Address</label>
                </div>
                
            </form>
        </div>        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success">Add Address</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>          
        </div>        
      </div>
    </div>
  </div>  
</div>
<!--/ Add New Address -->
</body>
</html>