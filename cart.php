<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cart list</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">My Cart</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>My Cart List </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3">
                        <div class="row">                    
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-2">Cart Items (6)</h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                <h5>Rs: 3,000.00</h5>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-9 col-md-7">
                            <!-- row -->
                            <div class="row">
                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-3">
                                        <div class="row">
                                            <div class="col-lg-3  col-sm-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 pl-0">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="productdetail.php">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6 col-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>
                                                    <div class="col-lg-6 col-6">
                                                        <select class="float-right">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Remove</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-heartwhite icomoon"></span>Add to wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-3">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 pl-0 col-sm-9">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="productdetail.php">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6 col-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>
                                                    <div class="col-lg-6 col-6">
                                                        <select class="float-right">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Remove</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-heartwhite icomoon"></span>Add to wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-3">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 pl-0">
                                                <p class="fgreen text-right">Delivery by 2th Jan 2019 </p>
                                                <article>
                                                    <h5><a href="productdetail.php">Gift Article Item Name here</a></h5>
                                                    <p>Orchidembroidery home furnishing articles a screen gifts gift jiangnan gift.</p>
                                                </article>
                                                
                                                <div class="row mpx-15">
                                                    <div class="col-lg-6 col-6">
                                                        <span class="price">Rs: 3,200</span>
                                                    </div>
                                                    <div class="col-lg-6 col-6">
                                                        <select class="float-right">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Remove</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-heartwhite icomoon"></span>Add to wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                        <div class="col-lg-3 col-md-5">
                            <div class="rtcart">
                                <h6 class="pb-2">Cart Items (6 Items)</h6>
                                <ul>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>                                    
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>                                        
                                        <p class="pb-0"><span class="fgreen fbold">Total: 	</span> <span class="float-right  fgreen">Rs. 8,880</span></p>
                                        <p class="pb-0"><span>Delivery Charges:  	</span> <span class="float-right">+Rs. 99</span></p>
                                    </li>
                                </ul>
                                <h4 class="fgreen fbold py-2 h4">You Pay <span class="float-right fgreen fbold">Rs. 8,880</span></h4>
                                <a href="productlist.php" class="greenlink w-100">Continue Shopping</a>
                                <a href="deliveryaddrerss.php" class="greenlink w-100 my-2">Checkout</a>
                                <p class="py-3">The price and availability of items at Vdesiconnect are subject to change. The shopping cart is a temporary place to store a list of your items and reflects each item's most recent price.</p>
                            </div>
                        </div>
                        <!--/ right cart -->
                    </div>
                    <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>