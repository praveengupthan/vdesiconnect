<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body> 
        <!--header -->
        <?php include 'header.php' ?>
        <!--/ header-->

       <main>
       <!-- div login -->
       <div class="sign">
           <div class="signin w-100">
                <div class="brandlogo text-center">
                    <a href="index.php"><img src="img/logo.svg" alt="" title="" class="img-fluid"></a>
                </div>
                <article class="text-center">
                    <h5 class="pb-1">Signin</h5>
                    <p>Enter your details below</p>
                </article>
                <form class="pt-4">
                    <div class="form-group">
                        <label>Email Address<span class="mand">*</span></label>
                        <input type="text" placeholder="Enter Your Email Address" class="form-control">
                    </div>
                    <div class="form-group mb-0">
                        <label>Password<span class="mand">*</span></label>
                        <input type="password" placeholder="Enter Your Email Address" class="form-control">
                    </div>
                    <p class="text-right"><a href="forgotpassword.php">Forgot Password?</a></p>
                    <input type="button" value="login" class="btn w-100 my-3" onclick="window.location.href='user-profileinformation.php'">
                    <p class="text-center">Don’t Have an Account? <a href="register.php" class="fgreen">Sign up</a></p> 
                </form>
           </div>
       </div>
       <!--/ div login -->
       </main>
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>