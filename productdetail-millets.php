<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Product Detail Name will be here</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Millet Name</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="productlist-millets.php">Millet Category</a></li>
                                <li><a>Millet Name</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                  <!-- page detail top block -->
                  <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <section class="simplegallery gallery">
                                <div class="content">
                                    <img src="img/data/millets/millets01.jpg" class="image_1" alt="" />
                                    <img src="img/data/millets/millets02.jpg" class="image_2" style="display:none" alt="" />
                                    <img src="img/data/millets/millets03.jpg" class="image_3" style="display:none" alt="" />
                                    <img src="img/data/millets/millets04.jpg" class="image_4" style="display:none" alt="" />
                                    <img src="img/data/millets/millets05.jpg" class="image_5" style="display:none" alt="" />                                    
                                </div>          

                                <div class="thumbnail">
                                    <div class="thumb">
                                        <a href="#" rel="1">
                                            <img src="img/data/millets/millets01.jpg" id="thumb_1" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#" rel="2">
                                            <img src="img/data/millets/millets02.jpg" id="thumb_2" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#" rel="3">
                                            <img src="img/data/millets/millets03.jpg" id="thumb_3" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#" rel="4">
                                            <img src="img/data/millets/millets04.jpg" id="thumb_4" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#" rel="5">
                                            <img src="img/data/millets/millets05.jpg" id="thumb_5" alt="" />
                                        </a>
                                    </div>
                                    
                                </div>
                            </section>  
                        </div>
                        <!--/ col -->
                        <!-- col 6-->
                        <div class="col-lg-6">
                            <div class="productbasic-info">
                                <h2>Foxtail Millet / Italian (Thinai Rice)  </h2>
                                <div class="priceproduct pt-3">
                                    <h3>
                                        <span class="mainprice">Rs:9,192.00</span> 
                                        <span class="oldprice">Rs:12,760.00</span> 
                                        <span class="percentage">-25%</span>                                    
                                    </h3>
                                </div>
                                <!--
                                <ul class="nav productrate pb-2">
                                    <li><a href="javascript:void(0)"><img src="img/svg/star.svg"></a></li>
                                    <li><a href="javascript:void(0)"><img src="img/svg/star.svg"></a></li>
                                    <li><a href="javascript:void(0)"><img src="img/svg/star.svg"></a></li>
                                    <li><a href="javascript:void(0)"><img src="img/svg/star.svg"></a></li>
                                    <li><a href="javascript:void(0)"><img src="img/svg/stargray.svg"></a></li>
                                </ul>
                                -->                                
                                <table class="detailtable">
                                    <tr>
                                        <td>Product Code</td>
                                        <td>:</td>
                                        <td> P080547</td>
                                    </tr>
                                    <tr>
                                        <td>Quantity	</td>
                                        <td>:</td>
                                        <td> 
                                            <select style="width:50px;">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Delivery Charges </td>
                                        <td>:</td>
                                        <td> Rs: 250</td>
                                    </tr>                                   
                                    <tr>
                                        <td>Product Type </td>
                                        <td>:</td>
                                        <td> Organic Food</td>
                                    </tr>                                   
                                    
                                    <tr>
                                        <td>Availability </td>
                                        <td>:</td>
                                        <td> <span class="fgreen">In Stock</span></td>
                                    </tr>
                                    <tr>
                                        <td>Delivery Date & Time </td>
                                        <td>:</td>
                                        <td> 
                                        <div class="form-group">
                                            <input type="text" placeholder="mm/dd/yyyy" class="form-control w-50 float-left">
                                            <input type="text" placeholder="Ex:2PM" class="form-control w-25 ml-2">
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                                <p class="pb-0">Write Custom Message On the gift to show on tag </p>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder=" Ex:with Love your Kumar">

                                    </textarea>
                                </div>
                                <!-- button -->
                                <div class="buttonsgroup pb-2">
                                    <button data-toggle="modal" data-target="#addtocart"><span class="icon-online-shopping-cart icomoon"></span>Add to cart</button>
                                    <a href="cart.php" class="greenlink">Gift Now</a>
                                    <button data-toggle="modal" data-target="#addtowlist"><span class="icon-heartwhite icomoon"></span>Add to wishlist</button>
                                </div>
                                <!--/ buttons -->
                                <a href="javascript:void(0)"><img src="img/socialall.png"></a>
                            </div>
                        </div>
                        <!--/ col 6 -->
                  </div>
                  <!--/ page detail top block -->
                  <!-- product detail tab -->
                  <div class="row">
                        <div class="col-lg-12">
                            <div class="producttab">
                                <div class="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1">
                                        <li>Overview</li>
                                        <li>Specs</li>
                                        <li>Quality Information</li>                                        
                                    </ul>
                                    <div class="resp-tabs-container hor_1">
                                        <!-- product review -->
                                        <div>
                                            <!-- row -->
                                           <div class="row">
                                                <div class="col-lg-4 col-md-5">
                                                    <img src="img/data/millets/millets02.jpg" alt="" title="" class="img-fluid">
                                                </div>
                                                <div class="col-lg-8 col-md-7 align-self-center">
                                                    <h3 class="mb-3">Product Overview</h3>
                                                    <p class="text-justify">Made with Fresh Whipped Cream and blends of Chocolate, Blackforest is the most sought-after cake flavour, and an all-time favorite of almost everyone. Send this 1 Kg freshly baked delicacy to your loved ones today. Suitable for Birthday, Anniversaries, and more occasions, etc.</p>
                                                    <p class="text-justify">Looking for a perfect way to make an occasion special? This Chocolate Truffle is all you need to add a special touch. The mushy chocolatey base finished perfectly with chocolate cream and artistic design is sure to please everyone in the crowd.</p>
                                                </div>
                                           </div>
                                           <!--/ row -->
                                        </div>
                                        <!--/ product review -->
                                         <!--product Speficifations-->
                                         <div>
                                            <h3 class="mb-3">Product Specifications</h3>
                                            <table class="spectable">
                                                <tr>
                                                    <td>Size</td>
                                                    <td>:</td>
                                                    <td>230 MM</td>
                                                </tr>
                                                <tr>
                                                    <td>Weight</td>
                                                    <td>:</td>
                                                    <td>1 Kelo Gram</td>
                                                </tr>
                                                <tr>
                                                    <td>Colors</td>
                                                    <td>:</td>
                                                    <td>Hot Pink</td>
                                                </tr>
                                                <tr>
                                                    <td>Type of</td>
                                                    <td>:</td>
                                                    <td>Cooling Pastry</td>
                                                </tr>
                                                <tr>
                                                    <td>Flavor</td>
                                                    <td>:</td>
                                                    <td>Pinappel</td>
                                                </tr>
                                                <tr>
                                                    <td>Size</td>
                                                    <td>:</td>
                                                    <td>230 MM</td>
                                                </tr>
                                                <tr>
                                                    <td>Weight</td>
                                                    <td>:</td>
                                                    <td>1 Kelo Gram</td>
                                                </tr>
                                                <tr>
                                                    <td>Colors</td>
                                                    <td>:</td>
                                                    <td>Hot Pink</td>
                                                </tr>
                                                <tr>
                                                    <td>Type of</td>
                                                    <td>:</td>
                                                    <td>Cooling Pastry</td>
                                                </tr>
                                                <tr>
                                                    <td>Product Description</td>
                                                    <td>:</td>
                                                    <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim, totam non. Doloribus incidunt laudantium autem, beatae eveniet quo error dolores?</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--/ product Speficifations -->
                                         <!--Quality Information-->
                                         <div>
                                            <h3 class="mb-3">Quality Care Information</h3>
                                            <p class="text-justify">This site is controlled and operated by Quality Foods from its offices within the Province of British Columbia, Canada. Those who choose to access this site from other locations do so on their own initiative and are responsible for compliance with laws governing export of hardware and software and other applicable laws, including local laws, if and to the extent local laws are applicable. </p>
                                            <p class="text-justify">Although the ShopQF Website is accessible worldwide, not all products on the ShopQF Website are available to all persons or in all geographic locations or jurisdictions. Quality Foods reserves the right to limit the availability of the ShopQF Website and/or the provision of any product to any person, to geographic area or to any jurisdiction it so desires, in its sole discretion, and to limit the quantities of any such product or service that it provides. Any request or offer for any product made on the ShopQF Website is void in any jurisdiction where such is prohibited. </p>
                                            <p class="text-justify">This site is controlled and operated by Quality Foods from its offices within the Province of British Columbia, Canada. Those who choose to access this site from other locations do so on their own initiative and are responsible for compliance with laws governing export of hardware and software and other applicable laws, including local laws, if and to the extent local laws are applicable. </p>
                                        </div>
                                        <!--/ Quality Information -->                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                  <!--/ product detail tab -->
                  <!-- related products -->
                  <section class="relatedproducts">
                       <!-- title row -->
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <article class="hometitle">
                                    <h3 class="px20 py20">You May Also Like</h3>
                                </article>
                            </div>
                        </div>
                        <!--/ title row -->
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-3 col-6 col-md-4 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img src="img/data/millets/millets01.jpg" alt="" title="" class="img-fluid"></a>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartwhite icomoon"></span></a></li>
                                                <li><a href="productdetail-millets.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail-millets.php">Millet Name</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 col-md-4 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img src="img/data/millets/millets02.jpg" alt="" title="" class="img-fluid"></a>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartwhite icomoon"></span></a></li>
                                                <li><a href="productdetail-millets.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail-millets.php">Millet Name</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 col-md-4 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img src="img/data/millets/millets03.jpg" alt="" title="" class="img-fluid"></a>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartwhite icomoon"></span></a></li>
                                                <li><a href="productdetail-millets.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail-millets.php">Millet Name</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 col-md-4 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img src="img/data/millets/millets04.jpg" alt="" title="" class="img-fluid"></a>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartwhite icomoon"></span></a></li>
                                                <li><a href="productdetail-millets.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail-millets.php">Millet Name</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                  </section>
                  <!--/ related products -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->   
    
    <!--/ pupup for write review -->
    <div class="modal fade" id="reviewpopup">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Write Review</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <form class="formreview">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" class="form-control">
                </div>
                <div class="form-group">
                    <label>Write Review</label>
                    <textarea class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>Select Rating</label>
                    <select class="form-control">
                        <option>Star 1</option>
                        <option>Star 2</option>
                        <option>Star 3</option>
                        <option>Star 4</option>
                        <option>Star 5</option>
                    </select>
                </div>
            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success">Submit Review</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>          
        </div>        
      </div>
    </div>
  </div>
  
</div>
<!--/ popup for write review -->

<!-- small popup for confirmation message add to kart -->
<!-- The Modal -->
<div class="modal" id="addtocart">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Success</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       Your Product Added to Your Cart List Successfully
      </div>    

    </div>
  </div>
</div>
<!--/ small popup for confirmation message add to kart-->

<!-- small popup for Add to wishlist -->
<!-- The Modal -->
<div class="modal" id="addtowlist">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Success</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Product Added to Your Wish List Successfully
      </div>    

    </div>
  </div>
</div>
<!--/ small popup for Add to wishlist-->


</body>
</html>