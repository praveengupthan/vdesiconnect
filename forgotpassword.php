<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body> 
        <!--header -->
        <?php include 'header.php' ?>
        <!--/ header-->
        <main>
        <!-- div login -->
        <div class="sign mx-auto">
            <div class="signin w-100">
                    <div class="brandlogo text-center">
                        <a href="index.php"><img src="img/logo.svg" alt="" title="" class="img-fluid"></a>
                    </div>
                    <article class="text-center">
                        <h5 class="pb-1">Forgot Password</h5>
                        <p>Enter your details below</p>
                    </article>
                    <form class="pt-4">
                        <div class="form-group">
                            <label>Registered Email Address<span class="mand">*</span></label>
                            <input type="text" placeholder="Enter Your Registered Email Address" class="form-control">
                        </div>     
                        
                        <p class="text-center msg">You get Recet Password link to your Registered Email Once you will click on below link</p>
                        
                        <input type="submit" value="RESET PASSWORD" class="btn w-100 my-3">
                        <p class="text-center">Back to <a href="login.php" class="fgreen">Sign in</a></p>
                    </form>
            </div>
        </div>
        <!--/ div login -->
       </main>
       <!--footer -->
        <?php include 'footer.php' ?>
        <!--/ footer -->
        <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>