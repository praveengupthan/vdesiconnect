<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Privacy Policy of Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Privacy Policy</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Privacy Policy </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <p class="text-justify">By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by Snapdeal as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person. </p>

                            <p class="text-justify">asper Infotech Private Limited and its subsidiaries and affiliates and Associate Companies (individually and/ or collectively, "Snapdeal") is/are concerned about the privacy of the data and information of users (including sellers and buyers/customers whether registered or non-registered) accessing, offering, selling or purchasing products or services on Snapdeal's websites, mobile sites or mobile applications ("Website") on the Website and otherwise doing business with Snapdeal. "Associate Companies" here shall have the same meaning as ascribed in Companies Act, 2013.</p>

                            <p class="text-justify">The terms "We" / "Us" / "Our" individually and collectively refer to each entity being part of the definition of Snapdeal and the terms "You" /"Your" / "Yourself" refer to the users. This Privacy Policy is a contract between You and the respective Snapdeal entity whose Website You use or access or You otherwise deal with. This Privacy Policy shall be read together with the respective Terms Of Use or other terms and condition of the respective Snapdeal entity and its respective Website or nature of business of the Website.</p>

                            <p class="text-justify">Non-Personal Information: Snapdeal may also collect information other than Personal Information from You through the Website when You visit and / or use the Website. Such information may be stored in server logs. This Non-Personal Information would not assist Snapdeal to identify You personally. This Non-Personal Information may include</p>

                            <p class="text-justify">The duration of Your stay on the Website is also stored in the session along with the date and time of Your access, Non-Personal Information is collected through various ways such through the use of cookies. Snapdeal may store temporary or permanent ‘cookies' on Your computer. You can erase or choose to block these cookies from Your computer. You can configure Your computer's browser to alert You when we attempt to send You a cookie with an option to accept or refuse the cookie. If You have turned cookies off, You may be prevented from using certain features of the Website.</p>

                            <p class="text-justify">Ads: Snapdeal may use third-party service providers to serve ads on Snapdeal's behalf across the internet and sometimes on the Website. They may collect Non-Personal Information about Your visits to the Website, and Your interaction with our products and services on the Website. Please do note that Personal Information and Non Personal Information may be treated differently as per this Privacy Policy.</p>

                            <h5 class="sectitle py-3">Sharing and disclosure <span class="fbold">of Your Information</span></h5>
                            <p class="text-justify">You hereby unconditionally agree and permit that Snapdeal may transfer, share, disclose or part with all or any of Your Information, within and outside of the Republic of India to various Snapdeal entities and to third party service providers / partners / banks and financial institutions for one or more of the Purposes or as may be required by applicable law. In such case we will contractually oblige the receiving parties of the Information to ensure the same level of data protection that is adhered to by Snapdeal under applicable law.</p>

                            <p class="text-justify">You acknowledge and agree that, to the extent permissible under applicable laws, it is adequate that when Snapdeal transfers Your Information to any other entity within or outside Your country of residence, Snapdeal will place contractual obligations on the transferee which will oblige the transferee to adhere to the provisions of this Privacy Policy.</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <h5 class="sectitle py-3">Vdesi connect has provided this <span class="fbold"> Privacy Policy to familiarise You with:</span></h5>
                            <ul class="pagelist">
                                <li>The type of data or information that You share with or provide to Snapdeal and that Snapdeal collects from You;</li>
                                <li>The purpose for collection of such data or information from You;</li>
                                <li>Snapdeal's information security practices and policies; and</li>
                                <li>Snapdeal's policy on sharing or transferring Your data or information with third parties. This Privacy Policy may be amended / updated from time to time. Upon amending / updating the Privacy Policy, we will accordingly amend the date above. We suggest that you regularly check this Privacy Policy to apprise yourself of any updates. Your continued use of Website or provision of data or information thereafter will imply Your unconditional acceptance of such updates to this Privacy Policy.</li>
                                <li>Information collected and storage of such Information: The "Information" (which shall also include data) provided by You to Snapdeal or collected from You by Snapdeal may consist of "Personal Information" and "Non-Personal Information".</li>
                                <li>Personal Information: Personal Information is Information collected that can be used to uniquely identify or contact You. Personal Information for the purposes of this Privacy Policy shall include, but not be limited to:</li>
                            </ul>
                            <h5 class="sectitle py-3">You hereby represent <span class="fbold">to Snapdeal that:</span></h5>
                            <ul class="pagelist">
                                <li>the Information you provide to Snapdeal from time to time is and shall be authentic, correct, current and updated and You have all the rights, permissions and consents as may be required to provide such Information to Snapdeal.</li>
                                <li>Your providing the Information to Snapdeal and Snapdeal's consequent storage, collection, usage, transfer, access or processing of the same shall not be in violation of any third party agreement, laws, charter documents, judgments, orders and decrees.</li>
                                <li>Snapdeal and each of Snapdeal entities officers, directors, contractors or agents shall not be responsible for the authenticity of the Information that You or any other user provide to Snapdeal. You shall indemnify and hold harmless Snapdeal and each of Snapdeal entities officers, directors, contracts or agents and any third party relying on the Information provided by You in the event You are in breach of this Privacy Policy including this provision and the immediately preceding provision above.</li>
                                <li>Your Information will primarily be stored in electronic form however certain data can also be stored in physical form. We may store, collect, process and use your data in countries other than Republic of India but under compliance with applicable laws. We may enter into agreements with third parties (in or outside of India) to store or process your information or data. These third parties may have their own security standards to safeguard your information or data and we will on commercial reasonable basis require from such third parties to adopt reasonable security standards to safeguard your information / data.</li>
                                <li>Purpose for collecting, using, storing and processing Your Information Snapdeal collects, uses, stores and processes Your Information for any purpose as may be permissible under applicable laws (including where the applicable law provides for such collection, usage, storage or processes in accordance with the consent of the user) connected with a function or activity of each</li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>