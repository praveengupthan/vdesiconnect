<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Make Payment</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Make a Payment</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="revieworder.php">Review Order </a></li>                              
                                <li><a>Make a Payment </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="whiterow mb-3">
                        <div class="row">                    
                            <div class="col-lg-6">
                                <h4 class="pl-2">Your Cart Items (6)</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <h5 class="pr-2">Rs: 3,000.00</h5>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-9">
                            <!-- row -->
                            <div class="row">
                            <div class="col-lg-12">
                               <!-- payment tab -->
                               <div class="whitebox p-3  w-100">                                    
                                        <div class="parentVerticalTab">
                                            <ul class="resp-tabs-list hor_1">
                                                <li><span class="icon-credit-card icomoon"></span>Debit Card</li>
                                                <li><span class="icon-credit-card icomoon"></span> Credit Card </li>
                                                <li><span class="icon-museum icomoon"></span>Net Banking</li>
                                                <li><span class="icon-purse icomoon"></span>Cash on Delivery</li>
                                            </ul>
                                            <div class="resp-tabs-container hor_1">
                                                <!-- debit card payment -->
                                                <div> 
                                                    <h5 class="h5">Pay Using Debit Card</h5>
                                                    <form class="w-75">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="Enter Card Number" class="form-control">
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <select class="form-control ">
                                                                <option>MM</option>
                                                                <option>January</option>
                                                                <option>February</option>
                                                                <option>March</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                                <option>August</option>
                                                                <option>September</option>
                                                                <option>October</option>
                                                                <option>Novembeer</option>
                                                                <option>December</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left mx-2">
                                                            <select class="form-control ">
                                                                <option>YY</option>
                                                                <option>2011</option>
                                                                <option>2012</option>
                                                                <option>2013</option>
                                                                <option>2014</option>
                                                                <option>2015</option>
                                                                <option>2016</option>
                                                                <option>2017</option>
                                                                <option>2018</option>
                                                                <option>2019</option>
                                                                <option>2020</option>
                                                                <option>2021</option>
                                                                <option>2022</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <input type="text" placeholder="CVV" class="form-control">
                                                        </div>

                                                        <div class="form-group w-100">
                                                            <input type="text" placeholder="Name on Card Holder" class="form-control">
                                                        </div>
                                                        <h2 class="h4 fbold">Your Pay <span class="fgreen float-right">Rs: 8,800</span></h2>
                                                        <a href="payment-success.php"class="greenlink">Submit</a>
                                                        <p class="small py-2">This card will be saved for a faster payment experience</p>

                                                    </form>
                                                </div>
                                                <!--/ debit card payment -->
                                                <!-- credit card payment -->
                                                <div> 
                                                <h5 class="h5">Pay Using Credit Card</h5>
                                                    <form class="w-75">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="Enter Card Number" class="form-control">
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <select class="form-control ">
                                                                <option>MM</option>
                                                                <option>January</option>
                                                                <option>February</option>
                                                                <option>March</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                                <option>August</option>
                                                                <option>September</option>
                                                                <option>October</option>
                                                                <option>Novembeer</option>
                                                                <option>December</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left mx-2">
                                                            <select class="form-control ">
                                                                <option>YY</option>
                                                                <option>2011</option>
                                                                <option>2012</option>
                                                                <option>2013</option>
                                                                <option>2014</option>
                                                                <option>2015</option>
                                                                <option>2016</option>
                                                                <option>2017</option>
                                                                <option>2018</option>
                                                                <option>2019</option>
                                                                <option>2020</option>
                                                                <option>2021</option>
                                                                <option>2022</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <input type="text" placeholder="CVV" class="form-control">
                                                        </div>

                                                        <div class="form-group w-100">
                                                            <input type="text" placeholder="Name on Card Holder" class="form-control">
                                                        </div>
                                                        <h2 class="h4 fbold">Your Pay <span class="fgreen float-right">Rs: 8,800</span></h2>
                                                        <a href="javascript:void(0)"class="greenlink">Proceed Payment</a>
                                                        <p class="small py-2">This card will be saved for a faster payment experience</p>
                                                    </form>
                                                </div>
                                                <!--/ credit card payment -->
                                                <!-- net banking -->
                                                <div> 
                                                <h5 class="h5">Pay Net Banking</h5>
                                                    <form class="w-75">
                                                        <div class="form-group">
                                                            <input type="text" placeholder="Enter Card Number" class="form-control">
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <select class="form-control ">
                                                                <option>MM</option>
                                                                <option>January</option>
                                                                <option>February</option>
                                                                <option>March</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                                <option>August</option>
                                                                <option>September</option>
                                                                <option>October</option>
                                                                <option>Novembeer</option>
                                                                <option>December</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left mx-2">
                                                            <select class="form-control ">
                                                                <option>YY</option>
                                                                <option>2011</option>
                                                                <option>2012</option>
                                                                <option>2013</option>
                                                                <option>2014</option>
                                                                <option>2015</option>
                                                                <option>2016</option>
                                                                <option>2017</option>
                                                                <option>2018</option>
                                                                <option>2019</option>
                                                                <option>2020</option>
                                                                <option>2021</option>
                                                                <option>2022</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group w-25 float-left">
                                                            <input type="text" placeholder="CVV" class="form-control">
                                                        </div>

                                                        <div class="form-group w-100">
                                                            <input type="text" placeholder="Name on Card Holder" class="form-control">
                                                        </div>
                                                        <h2 class="h4 fbold">Your Pay <span class="fgreen float-right">Rs: 8,800</span></h2>
                                                        <a href="javascript:void(0)"class="greenlink">Proceed Payment</a>
                                                        <p class="small py-2">This card will be saved for a faster payment experience</p>
                                                    </form>
                                                </div>
                                                <!--/ net banking -->
                                                <!-- cash on delivery -->
                                                <div>
                                                    <h5 class="h5">Cash on Delivery</h5>
                                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur sed, eligendi distinctio voluptatem illo omnis temporibus adipisci praesentium totam. Rem.</p>
                                                </div>
                                                <!--/ cash on delivery -->
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <!--/ payment tab -->                             
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                        <div class="col-lg-3">
                            <div class="rtcart">
                                <h6 class="pb-2">Cart Items (6 Items)</h6>
                                <ul>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>                                    
                                    <li>
                                        <p class="pb-0">Gift Name will be here</p>
                                        <p><span>Quantity :1	</span> <span class="float-right">Rs: 8,800</span></p>
                                    </li>
                                    <li>                                        
                                        <p class="pb-0"><span class="fgreen fbold">Total: 	</span> <span class="float-right  fgreen">Rs. 8,880</span></p>
                                        <p class="pb-0"><span>Delivery Charges:  	</span> <span class="float-right">+Rs. 99</span></p>
                                    </li>
                                </ul>
                                <h4 class="fgreen fbold py-2 h4">You Pay <span class="float-right fgreen fbold">Rs. 8,880</span></h4>
                            </div>
                        </div>
                        <!--/ right cart -->
                    </div>
                    <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>