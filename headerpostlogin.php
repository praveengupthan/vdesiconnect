<header class="fixed-top">
    <!-- top header -->
    <section class="topheader">
        <section class="headercontainer">
            <!-- container fluid-->
            <div class="container-fluid">
                <!-- row  -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-10 align-self-center">
                        <ul class="nav leftnav">
                                                      
                            <li class="nav-item">Call us +1800-621-3294</li>
                        </ul>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-9 pr-0">
                        <nav class="navbar navbar-expand-lg navbar-light">                               
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavtop" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-menu icomoon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavtop">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="index.php">Home </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="citieswedeliver.php">Cities We Deliver</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="href="http://vdesiconnect.com/vendor/" target="_blank"">Become Vendor </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="contact.php">Contact</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="bloglist.php">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="faq.php">Faq</a>
                                    </li>
                                    <li class="nav-item social">
                                        <a href="javascript:void(0)"><span class="icon-facebook-logo icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-twitter-logo icomoon"></span></a>
                                        <a href="javascript:void(0)"><span class="icon-linkedin-logo icomoon"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid-->
        </section>
    </section>
    <!--/ top header -->
     <!-- middle header -->
     <section class="middle-header">
        <section class="headercontainer">
            <div class="container-fluid">
                <nav class="navbar navbar-light px-0 py-0">
                    <!-- row -->
                    <div class="row w-100">
                        <div class="col-lg-3">
                        <a class="navbar-brand pb-0" href="index.php"><img src="img/logo.svg" alt="" title=""></a>            </div>     
                        <div class="col-lg-4 align-self-center dmnone">
                            <!-- sarch -->
                            <div class="headersearch">
                                <input type="text" placeholder="Search Your Gift Here">
                                <input type="submit" value="Search">
                            </div>
                            <!--/ search -->
                        </div>
                        <div class="col-lg-5 text-right align-self-center pr-0">
                            <div class="navbar-collapse" id="navbarNavDropdown">
                                <ul class="navbar-nav nav">                               
                                    <li class="nav-item">
                                        <a class="nav-link" href="cart.php">
                                        <span class="icon-online-shopping-cart icomoon"></span>Cart <span class="vol">3</span> </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="login.php"><span class="icon-heartwhite icomoon"></span> Wishlist <span class="vol">5</span> </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="icon-userprofile icomoon"></span> User Name
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item" href="user-profileinformation.php">Profile Information</a>
                                            <a class="dropdown-item" href="user-changepassword.php">Change Password</a>
                                            <a class="dropdown-item" href="user-myorders.php">My Orders</a>
                                            <a class="dropdown-item" href="user-wishlist.php">My Wishlist</a>
                                            <a class="dropdown-item" href="index.php">Logout</a>                                 
                                        </div>
                                    </li>                              
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                </nav>
            </div>
        </section>
    </section>
    <!-- middle header -->
    <!-- bottom navbar -->
    <section class="bottomnavbar">
        <section class="headercontainer">
            <!-- nav bar-->
            <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
                <div class="container-fluid pl-0">
                <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler btn-template-outlined"><span class="icon-menu icomoon"></span>Product Navigation</span><i class="fa fa-align-justify"></i></button>
                    <div id="navigation" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">                                  
                        <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Cakes<b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                <div class="col-lg-6"><img src="img/data/cakes-navigation-image.jpg" alt="" class="img-fluid d-none d-lg-block"></div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By Type</h5>
                                <ul class="list-unstyled mb-3">
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">All Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Delicious Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Designer Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Photo Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Eggless Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Heart Shaped Cakes</a></li> 
                                </ul>
                                </div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By Flavour</h5>
                                <ul class="list-unstyled mb-3">
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Black Forest Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Butterscotch Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Chocklate Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Fruit Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Pineapple Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Red Velvet Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Coffee Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Strawberry Cakes</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Vanilla Cakes</a></li> 
                                </ul>
                                </div>
                            </div>
                            </li>
                        </ul>
                        </li>
                        <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Flower Bouquets <b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                <div class="col-lg-6"><img src="img/data/flowers-boutique-navigation-img.jpg" alt="" class="img-fluid d-none d-lg-block">
                                </div>                                   
                                <div class="col-lg-2 col-md-6">
                                    <h5>By Occassion</h5>
                                    <ul class="list-unstyled mb-3">
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Anniversay</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Birthday</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Appreciation</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Congratulations</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Get Wellsoon</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Love &amp; Affection</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Thank You</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-md-6">
                                    <h5>By Type</h5>
                                    <ul class="list-unstyled mb-3">
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Roses</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Carnations</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Gerberas</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Lillies</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Orchids</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Mixed Flowers</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Custom Flowers</a></li>
                                    </ul>                                    
                                </div>                                   
                            </div>
                            </li>
                        </ul>
                        </li>                           
                        <!-- ========== Boutique ==================-->
                            <!-- ========== gifts start ==================-->
                        <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Gifts<b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                    <div class="col-lg-6"><img src="img/data/gifts-navigation-image.jpg" alt="" class="img-fluid d-none d-lg-block"></div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By Type</h5>
                                <ul class="list-unstyled mb-3">
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">All Gifts</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Personalized Gifts</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Soft Toys</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Mugs</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Idols</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Cushions</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Plants</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Aprons</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Other & Misclenious</a></li>
                                </ul>
                                </div>
                                <div class="col-lg-2 col-md-6">                                    
                                    <h5>By Recipient</h5>
                                    <ul class="list-unstyled mb-3">
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Her</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Him</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Girl Friend</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Boy Friend</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Husband</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Friend</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Wife</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Mother</a></li>
                                        <li class="nav-item"><a href="productlist.php" class="nav-link">Father</a></li>
                                    </ul>                                    
                                </div>
                                <div class="col-lg-2 col-md-6">                                  
                                <ul class="list-unstyled mb-3">
                                <li class="nav-item"><a href="productlist.php" class="nav-link">Sister</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Brother</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Daughter</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Parents</a></li>
                                </ul>
                                
                                </div>
                            </div>
                            </li>
                        </ul>
                        </li>    
                        <!-- ========== gifts End ==================-->

                            <!-- ========== Chocolates start ==================-->
                            <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Chocolates<b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                    <div class="col-lg-6"><img src="img/data/chocklates-navigation-image.jpg" alt="" class="img-fluid d-none d-lg-block"></div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By Types</h5>
                                <ul class="list-unstyled mb-3">
                                <li class="nav-item"><a href="productlist.php" class="nav-link">All Chocklates</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Chocklate Bouquet</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Ferraro Rohar Chocklates</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Cadburry Chocklates</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Personalized Chocklates</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Handmade Chocklates</a></li>
                                </ul>
                                </div>
                            </div>
                            </li>
                        </ul>
                        </li>    
                        <!-- ========== Chocolates End ==================-->

                            <!-- ========== Jewellery start ==================-->
                            <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Jewellery<b class="caret"></b></a>
                            <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                    <div class="col-lg-6"><img src="img/data/jewellery-navigation-image.jpg" alt="" class="img-fluid d-none d-lg-block"></div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By Type</h5>
                                <ul class="list-unstyled mb-3">
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Fashion Jewellery</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Traditional Imitation</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Gold and Diamond</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Gold Coins</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Ear Rings</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Pendants</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">bangles &amp; Bracelets</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Rings</a></li> 
                                </ul>
                                </div>
                            </div>
                            </li>
                        </ul>
                        </li>    
                        <!-- ========== Jewellery End ==================-->

                        
                            <!-- ========== Millets start ==================-->
                            <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Millets<b class="caret"></b></a>
                            <ul class="dropdown-menu megamenu">
                            <li>
                            <div class="row">
                                    <div class="col-lg-6"><img src="img/data/millets-navigation-img.jpg" alt="" class="img-fluid d-none d-lg-block"></div>
                                <div class="col-lg-2 col-md-6">
                                <h5>By All</h5>
                                <ul class="list-unstyled mb-3">
                                <li class="nav-item"><a href="productlist.php" class="nav-link">Sorghum (Bili Jola)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Barnyard millet (Oodhalu)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Liitle millet (Saame)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Foxtail millet (Navane)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Kodo millet (Harka)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Finger millet (Ragi)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Pearl millet (Sajje)</a></li>
                                    <li class="nav-item"><a href="productlist.php" class="nav-link">Proso millet (Baragu)</a></li> 
                                </ul>
                                </div>
                                
                            </div>
                            </li>
                        </ul>
                        </li>    
                        <!-- ========== Millets End ==================-->                            
                        
                            <!-- ========== Professional Services ==================-->
                            <li class="nav-item dropdown"><a href="javascript: void(0)" data-toggle="dropdown" class="dropdown-toggle">Professional Services <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="medicalservices.php" class="nav-link">Medical Assistance</a></li>
                                <li class="dropdown-item"><a href="onlinetutor.php" class="nav-link">Online Tutor</a></li>
                                <li class="dropdown-item"><a href="propertymanagement.php" class="nav-link">Property Management</a></li>
                                <li class="dropdown-item"><a href="visaservices.php" class="nav-link">Visa Services (UK, US, UKE, etc)</a></li>                                    
                                <li class="dropdown-item"><a href="visitorsinsurance.php" class="nav-link">Insurance (Health &amp; Visitors)</a></li>
                                <li class="dropdown-item"><a href="onlineradio.php" class="nav-link">Online Radio</a></li>
                            </ul>
                        </li>
                        <!-- ========== Professional Services ==================-->                            
                    </ul>
                    </div>                       
                </div>
            </div>
            <!--/ nav bar -->                
        </section>
    </section>
    <!--/ bottom navbar-->
</header>