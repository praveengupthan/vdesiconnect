<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Contact Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Contact us</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Contact </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                      <!-- left column -->
                      <div class="col-lg-8 col-md-7"> 
                         <h5 class="sectitle pb-3">Drop us <span class="fbold">Message</span></h5>
                         <!-- form -->
                         <form class="formpage">
                             <!-- row -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="First Name" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Last Name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Email Address" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Phone Number" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Subject" class="form-control">
                                    </div>
                                </div>                               
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message </label>
                                        <textarea class="form-control">

                                        </textarea>
                                    </div>
                                </div>                               
                            </div>
                            <!--/ row -->
                            <div class="row">
                                <div class="col-lg-4">
                                    <input type="submit" value="Send Message" class="greenlink">
                                </div>
                            </div>
                         </form>
                         <!--/ form -->                         
                      </div>
                      <!--/ left column -->
                      <!-- right column -->
                      <div class="col-lg-4 col-md-5 mpt-15">
                            <div class="whitebox p-3">
                                <table class="table-contact">
                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span class="icon-email icomoon"></span></span>
                                        </td>
                                        <td>
                                            <h6 class="h6">Email</h6>
                                            <p>info@vdesiconnect.org</p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span class="icon-call icomoon"></span></span>
                                        </td>
                                        <td>
                                            <h6 class="h6">Phone Number</h6>
                                            <p>+91 9642123254</p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span class="icon-placeholder icomoon"></span></span>
                                        </td>
                                        <td>
                                            <h6 class="h6">Contact Address</h6>
                                            <p>Plot No: 10/2, Arora Colony, Housing B oard colony, bengaluru, Karnataka - 50072</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>                        
                      </div>
                      <!--/ right column -->

                      <div class="col-lg-12">
                        <div class="map mt-3">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15226.408319014055!2d78.43539315000001!3d17.43087385!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1550742172879" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                      </div>
                    </div>
                    <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>