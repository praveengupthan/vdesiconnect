<section class="saleproducts">
    <!-- container -->
    <div class="container">
            <!-- title row -->
            <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <article class="hometitle">
                    <h3 class="px20 py20">Sale Products</h3>
                </article>
            </div>
        </div>
        <!--/ title row -->
        <div class="row">
            <div class="col-lg-12">
                    <!-- tab products -->
                <div class="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>New Arrival</li>
                        <li>Best Seller</li>
                        <li>Featured</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <!-- new arrival-->
                            <div class="row">

                                <!-- col -->
                                <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/flowers/flower05.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag flowers">Flowers</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail-flower.php">Flower Name </a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake01.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Cakes</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/flowers/flower06.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag flowers">Flowers</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail-flower.php">Flower Name </a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake06.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag cakes">Cakes</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake08.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Cakes</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name </a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake07.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Chocklates</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/gifts/gift01.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag fashion">Gifts</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail-gift.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail-gift.php">Gift Name</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/jewellery/jewel01.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag fashion">Jewellery</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail-jewellery.php">Jewellery Name</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                            </div>
                            <!--/ new arrival -->
                        </div>
                        <div>
                            <!-- best selles -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake08.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Chocklates</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake07.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Chocklates</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/fashion/fashion06.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag fashion">Boutique</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/fashion/fashion05.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag fashion">Boutique</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->
                                </div>
                            <!--/ best sellers -->
                        </div>
                        <div>
                            <!-- featured-->
                            <div class="row">
                            <!-- col -->
                            <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/flowers/flower05.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag flowers">Flowers</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake07.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag chocklate">Chocklates</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/flowers/flower06.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag flowers">Flowers</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-3 col-6 text-center">
                                    <div class="productitem">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/cakes/cake06.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="cattag cakes">Cakes</span>
                                            <div class="hover">
                                                <ul class="nav">
                                                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                                    <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                                </ul>
                                            </div>
                                        </figure>
                                        <article>
                                            <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                            <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                            <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ featured -->
                        </div>
                    </div>
                </div>
                <!--/ tab products -->
            </div>
        </div>        
    </div>
    <!--/ container -->
</section>