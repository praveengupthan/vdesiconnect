 <script src="js/jquery-3.2.1.min.js"></script><!--jquery-->
    <script src="js/bootstrap.bundle.min.js"></script> <!-- bootstrap -->
    <script src="js/simplegallery.min.js"></script><!-- product detail gallery --> 
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/custom.js"></script> <!-- custom jquery-->
    <script src="js/accordian.js"></script><!-- accordian -->
    <script src="js/easyresponsivetabs.js"></script><!-- responsive tabs -->
    <script src="js/jquery.basictable.min.js"></script>
    <!--[if lt IE  9]>
    <script src="js/html5-shiv.js"></script>
    <![endif]-->  
     <!-- bootstrap calendar-->

<script>
//responsive tables
$(document).ready(function() {
    $('#table').basictable();

    $('#table-breakpoint').basictable({
      breakpoint: 768
    });

    $('#table-container-breakpoint').basictable({
      containerBreakpoint: 485
    });

    $('#table-swap-axis').basictable({
      swapAxis: true
    });

    $('#table-force-off').basictable({
      forceResponsive: false
    });

    $('#table-no-resize').basictable({
      noResize: true
    });

    $('#table-two-axis').basictable();

    $('#table-max-height').basictable({
      tableWrapper: true
    });
  });
  </script>
     