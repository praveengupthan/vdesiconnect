<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Privacy Policy of Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Terms of Use</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Terms of Use </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                       <div class="col-lg-12" >
                            <h5 class="sectitle  py-3"><span class="fbold">General</span></h5>
                            <p class="text-justify">Jasper Infotech is a company incorporated under the laws of India, with its registered office at C/o SproutBox Suryavilas, Suite #181-TR4 First Floor, D-181, Okhla Industrial Area, Phase 1, New Delhi 110020, India and having CIN U72300DL2007PTC168097. Jasper Infotech is an intermediary in the form of an online marketplace and is limited to managing the Website to enable seller to exhibit, advertise, display, make available and offer to sell the products and to enable buyer to purchase the products so offered ("Products"), and other incidental Services thereto ("Services") including use of the Website by any User.</p>
                            <p class="text-justify">These Terms of Use are subject to revision by Jasper Infotech at any time and hence the Users are requested to carefully read these Terms of Use from time to time before using the Website. The revised Terms of Use shall be made available on the Website. You are requested to regularly visit the Website to view the most current Terms of Use. In the event such a facility is provided on the Website, You can determine when Snapdeal last modified any part of the Agreement by referring to the "Last Updated" legend provided in that document. It shall be Your responsibility to check these Terms of Use periodically for changes. Snapdeal may require You to provide Your direct or indirect consent to any update in a specified manner before further use of the Website and the Services. If no such separate consent is sought, Your continued use of the Website and/or Services, following such changes, will constitute Your acceptance of those changes.</p>
                            <p class="text-justify">If there is any conflict: (i)	between the Privacy Policy and any other Agreement, the Privacy Policy shall take precedence' but only to the extent of the conflict; (ii)	between the Additional Service Terms and any other part of these Terms of Use, the Additional Service Terms shall take precedence in relation to that Service;
                            (iii)	between the Seller Agreement and any other part of these Terms of Use, the Seller Agreement shall take precedence but only to the extent of the conflict;  (iv)	between these Terms of Use and any other notices, disclaimers or guidelines appearing on the Website, these Terms of Use shall take precedence but only to the extent of the conflict.</p>

                        <h5 class="sectitle  py-3"><span class="fbold">Services</span></h5>
                        <p class="text-justify">The Website is an electronic platform in the form of an electronic marketplace and an intermediary that (a) provides a platform for Users (who are sellers) to advertise, exhibit, make available and offer to sell various Products to other Users (who are buyers / customers), and (b) a platform for such other Users to accept the offer to sell of the Products made by the sellers on the Website and to make payments to the sellers for purchase of the Products, and (c) services to facilitate the engagement of buyers and sellers to under commerce on the Website, and (d) such other services as are incidental and ancillary thereto. The Services are offered to the Users through various modes which may include issue of coupons and vouchers that can be redeemed for various Products.</p>

                        <h5 class="sectitle  py-3">Eligibility <span class="fbold"> to Use</span></h5>
                        <p class="text-justify">The Website is an electronic platform in the form of an electronic marketplace and an intermediary that (a) provides a platform for Users (who are sellers) to advertise, exhibit, make available and offer to sell various Products to other Users (who are buyers / customers), and (b) a platform for such other Users to accept the offer to sell of the Products made by the sellers on the Website and to make payments to the sellers for purchase of the Products, and (c) services to facilitate the engagement of buyers and sellers to under commerce on the Website, and (d) such other services as are incidental and ancillary thereto. The Services are offered to the Users through various modes which may include issue of coupons and vouchers that can be redeemed for various Products.</p>
                        <p class="text-justify">Snapdeal reserves the right to refuse access to use the Services offered at the Website to new Users or to terminate access granted to existing Users at any time without according any reasons for doing so.</p>
                        <p class="text-justify">You shall not have more than one active Account (defined hereunder) on the Website. Additionally, You are prohibited from selling, trading, or otherwise transferring Your Account to another person.</p>

                        <h5 class="sectitle  py-3">User Account, <span class="fbold"> Password, and Security</span></h5>
                        <p class="text-justify">You may access and use the Website and the Services either as a registered user or as a guest user. However, not all sections of the Website and Services will be accessible to guest users.</p>
                        <p class="text-justify">Registered users: Snapdeal makes certain sections of the Services available to You through the Website only if You have provided Snapdeal certain required User information and created an account and a Snapdeal ID through certain log-in ID and password ("Account"). You can create Your Account on the Website through logging in by Your third party website user ID and password including that of www.facebook.com, websites owned by Yahoo Inc. or its subsidiaries, Google Inc. or its subsidiaries, twitter or any other social media website or any other Internet service as permitted on the Website (Snapdeal ID and/or other third party login identification as provided above are individually and collectively referred to the "Account Information"). </p>
                        <p class="text-justify">In the event You register as a User by creating an Account in order to avail of the Services provided by the Website, You will be responsible for maintaining the confidentiality and security of the Account Information, and are fully responsible for all activities that occur under Your Account. You agree to (a) immediately notify Snapdeal of any unauthorized use of Your Account Information or any other breach of security, and (b) ensure that You exit from Your Account at the end of each session. Snapdeal cannot and will not be liable for any loss or damage arising from Your failure to comply with this section. You may be held liable for losses incurred by Snapdeal or any other user of or visitor to the Website due to authorized or unauthorized use of Your Account as a result of Your failure in keeping Your Account Information secure and confidential. </p>

                       </div>
                       
                    </div>
                    <!--/ row -->

               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>