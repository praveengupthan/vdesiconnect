<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sitemap</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Sitemap</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Sitemap </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                   <!-- row -->
                   <div class="row">


                       <!-- col -->
                       <div class="col-lg-12 py-3">
                            <h5 class="sectitle flight pb-1"> <span class="fbold">Company</span></h5>
                            <ul class="sitemaplinks nav">
                                <li><a href="about.php">About</a></li>
                                <li><a href="corevalues.php">Core Values</a></li>
                                <li><a href="bloglist.php">Blog</a></li>                            
                                <li><a href="career.php">Career</a></li>
                                <li><a href="sitemap.php">Sitemap</a></li>
                                <li><a href="contact.php">Contact us</a></li>
                                <li><a href="faq.php">Faq</a></li>
                                <li><a href="citieswedeliver.php">Cities We Deliver</a></li>
                            </ul>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-12 py-3">
                            <h5 class="sectitle flight pb-1"> Our <span class="fbold">Policies</span></h5>
                            <ul class="sitemaplinks nav">
                                <li><a href="privacy.php">Privacy Policy</a></li>
                                <li><a href="returnpolicy.php">Return Policy</a></li>
                                <li><a href="termsofuse.php">Terms of  Use</a></li>          
                            </ul>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-12 py-3">
                            <h5 class="sectitle flight pb-1"> Primary  <span class="fbold">Categories</span></h5>
                            <ul class="sitemaplinks nav">
                                <li><a href="javascript:void(0)">Cakes</a></li>
                                <li><a href="javascript:void(0)">Flowers & Bouquet</a></li>
                                <li><a href="javascript:void(0)">Dresses & Boutique</a></li>
                                <li><a href="javascript:void(0)">Jewellery</a></li>  
                                <li><a href="javascript:void(0)">Sweets</a></li>
                                <li><a href="javascript:void(0)">Chocklates</a></li>
                                <li><a href="javascript:void(0)">Gifts Articles</a></li>                      
                            </ul>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-12 py-3">
                            <h5 class="sectitle flight pb-1"> Our  <span class="fbold"> Services </span></h5>
                            <ul class="sitemaplinks nav">
                                <li><a href="onlinetutor.php">Online Tutors</a></li>
                                <li><a href="medicalservices.php">Medical Services</a></li>
                                <li><a href="visaservices.php">Visa Suport Services</a></li>
                                <li><a href="propertymanagement.php">Property Management</a></li>  
                                <li><a href="onlineradio.php">Online Radio</a></li>
                                <li><a href="visitorsinsurance.php">Visitors Insurance</a></li>                      
                            </ul>
                       </div>
                       <!--/ col -->


                   </div>
                   <!--/ row -->
               </div>
              <!--/ container -->           
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>