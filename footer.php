<footer>
        <!-- news letter footer -->
        <section class="newsletterfooter">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 mtcenter col-sm-8">
                        <h5>Subscribe News letter</h5>
                        <p>Good things come to those who sign up for our newsletter! </p>
                        <div class="newsform">
                            <div class="row">
                                <div class="col-lg-9">
                                    <input type="text" placeholder="Enter Your Email">
                                </div>
                                <div class="col-lg-3">
                                    <input type="submit" value="Subscribe">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 col-sm-4 mtcenter ">
                        <div class="footersocial">
                            <h5>Follow us on Social</h5>
                            <p>You Can Get keep updates through social media </p>
                            <ul class="nav">
                                <li><a href="javascript:void(0)" target="_blank"><span class="icon-facebook-logo icomoon"></span></a></li>
                                <li><a href="javascript:void(0)" target="_blank"><span class="icon-twitter-logo icomoon"></span></a></li>
                                <li><a href="javascript:void(0)" target="_blank"><span class="icon-linkedin-logo icomoon"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ news letter footer-->
        <!-- navigation footer -->
        <section class="navfooter py-3">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4 col-md-12 footercol">
                        <h5>Vdesi Connect </h5>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.     <a href="about.php"> Read more</a>
                        </p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-2 col-6 col-md-3 footercol">
                        <h5>Company </h5>
                        <ul>
                            <li><a href="about.php">About</a></li>
                            <li><a href="corevalues.php">Core Values</a></li>
                            <li><a href="bloglist.php">Blog</a></li>                            
                            <li><a href="career.php">Career</a></li>
                            <li><a href="sitemap.php">Sitemap</a></li>
                            <li><a href="contact.php">Contact us</a></li>
                            <li><a href="faq.php">Faq</a></li>
                            <li><a href="citieswedeliver.php">Cities We Deliver</a></li>
                        </ul>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-2 col-6 col-md-3 footercol">
                        <h5>OUR POLICIES </h5>
                        <ul>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="returnpolicy.php">Return Policy</a></li>
                            <li><a href="termsofuse.php">Terms of  Use</a></li>                                                       
                        </ul>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-2 col-6 col-md-3 footercol">
                        <h5>CATEGORIES </h5>
                        <ul>
                            <li><a href="productlist-cakes.php">Cakes</a></li>
                            <li><a href="productlist.php">Flowers Bouquet</a></li>
                            <li><a href="productlist-gifts.php">Gifts</a></li>
                            <li><a href="productlist-chocklates.php">Chocklates</a></li>  
                            <li><a href="productlist-jewellery.php">Jewellery</a></li>
                            <li><a href="productlist-millets.php">Millets</a></li> 
                        </ul>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-2 col-6 col-md-3 footercol">
                        <h5>Services </h5>
                        <ul>
                            <li><a href="onlinetutor.php">Online Tutors</a></li>
                            <li><a href="medicalservices.php">Medical Services</a></li>
                            <li><a href="visaservices.php">Visa Suport Services</a></li>
                            <li><a href="propertymanagement.php">Property Management</a></li>  
                            <li><a href="real-estateservices.php">Real Estate</a></li>
                            <li><a href="onlineradio.php">Online Radio</a></li>
                            <li><a href="visitorsinsurance.php">Visitors Insurance</a></li> 
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ navigation footer -->
        <!-- copy rights -->
        <div class="copyrights">
            <a id="movetop" class="" href="javascript:void(0)"><span class="icon-up-arrow-key icomoon"></span></a>
            <div class="container">
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 mtcenter col-md-6">
                        <p>All Rights & Copyright 2019 vdesiconnect.com</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 text-right dmnone">
                        <img src="img/cards.png" alt="" title="" class="img-fluid">
                    </div>
                    <!--/ col -->
                </div>
            </div>
        </div>
        <!--/ copty rights -->
    </footer>