<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register with us</title>
    <?php include 'headerstyles.php' ?>
</head>
<body> 
        <!--header -->
        <?php include 'header.php' ?>
        <!--/ header-->
        <main>
            <!-- div login -->
            <div class="sign mx-auto">
                <div class="signin w-100">
                        <div class="brandlogo text-center">
                            <a href="index.php"><img src="img/logo.svg" alt="" title="" class="img-fluid"></a>
                        </div>
                        <article class="text-center">
                            <h5 class="pb-1">Sign Up</h5>
                            <p>Enter your details below</p>
                        </article>
                        <form class="pt-4">
                            <div class="form-group">
                                <label>First Name <span class="mand">*</span></label>
                                <input type="text" placeholder="Enter Your First Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" placeholder="Enter Your Last Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Email Address<span class="mand">*</span></label>
                                <input type="text" placeholder="Enter Your Email Address" class="form-control">
                            </div>                   
                            <div class="form-group">
                                <label>Enter Password<span class="mand">*</span></label>
                                <input type="password" placeholder="Create Password" class="form-control">
                            </div>
                            <div class="form-group mb-0">
                                <label>Confirm Password<span class="mand">*</span></label>
                                <input type="password" placeholder="Confirm Password" class="form-control">
                            </div>                   
                            <input type="submit" value="Register With us" class="btn w-100 my-3">
                            <p class="text-center">Already have an account ? <a href="login.php" class="fgreen">Sign in</a></p>                    
                        </form>
                </div>
            </div>
            <!--/ div login -->
       </main>
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>