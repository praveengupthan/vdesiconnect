<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect Gift your Dear</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
        <!-- home page carousel -->
        <section class="homecarousel">
            <!-- main slider -->
            <div id="carouselExampleIndicators" class="carousel slide homeslider" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                 <!-- Slide One - Set the background image for this slide in the line below -->
                 <div class="carousel-item active" style="background-image: url('img/home-slider-cakes-bg.jpg')"> 
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/summerinrichment.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption  p-3">
                                    <h5 class="mb-3">Summer Seasonal Offer</h5>
                                    <h1 class="h1 fbold">Summer Enrichment</h1>
                                    <p class="fwhite">Feeling kids are Staying Home and Doing Nothing in this summer.  How about Summer Enrichment Programs at your convenience, available online, so no need to commute.</p>
                                </div>
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->                        
                    </div>
                    <!-- Slide One - Set the background image for this slide in the line below -->
                    <div class="carousel-item" style="background-image: url('img/home-slider-cakes-bg.jpg')"> 
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/home-slider-cakes-img.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption  p-3">
                                    <h5 class="mb-3">Seasonal Collection Sale</h5>
                                    <h1 class="h1 fbold">Celebrarte Love with Cakes</h1>
                                    <p class="fwhite">Simply Grown, Simply Beautiful, Delivery always included</p>
                                </div>
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->                        
                    </div>
                    <!-- Slide Two - Set the background image for this slide in the line below -->
                    <div class="carousel-item" style="background-image: url('img/home-slider-flowers-bg.jpg')"> 
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/home-slider-flowers-img.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption  p-3">
                                    <h5 class="mb-3">Seasonal Collection Sale</h5>
                                    <h2 class="h1 fbold">Flowers<span class=""> Bouquet </span></h2>
                                    <p class="fwhite">Simply Grown, Simply Beautiful, Delivery always included</p>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                    <!-- Slide Three - Set the background image for this slide in the line below -->
                    <div class="carousel-item" style="background-image: url('img/home-slider-gifts-bg.jpg')">  
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/home-slider-gifts-img.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption p-3">
                                    <h5 class="mb-3">Seasonal Collection Sale</h5>
                                    <h2 class="h1 fbold">Gift <span class="">Collections</span></h2>
                                    <p class="fwhite">Simply Grown, Simply Beautiful, Delivery always included</p>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                     <!-- Slide Four - Set the background image for this slide in the line below -->
                     <div class="carousel-item" style="background-image: url('img/home-slider-chocklates-bg.jpg')">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/home-slider-chocklates-img.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption p-3">
                                    <h5 class="mb-3">Seasonal Collection Sale</h5>
                                    <h2 class="h1 fbold">Express Love with <span class="">Chocklates</span></h2>
                                    <p class="fwhite">Simply Grown, Simply Beautiful, Delivery always included</p>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                      <!-- Slide Five - Set the background image for this slide in the line below -->
                      <div class="carousel-item" style="background-image: url('img/home-slider-jewellery-bg.jpg')"> 
                      <!-- row -->
                      <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 col-sm-6 align-self-center">
                                <img src="img/home-slider-jewellery-img.png" alt="" title="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4 col-sm-6 align-self-center">
                                <div class="carousel-caption p-3">
                                    <h5 class="mb-3">Seasonal Collection Sale</h5>
                                    <h2 class="h1 fbold">Express Love with <span class="">Jewellery</span></h2>
                                    <p class="fwhite">Simply Grown, Simply Beautiful, Delivery always included</p>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="icon-left-arrow-key icomoon"></span> <span class="sr-only">Previous</span> </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="icon-keyboard-right-arrow-button icomoon"></span> <span class="sr-only">Next</span> </a>
            </div>
            <!--/ main slider -->
        </section>
        <!--/ home page carousel -->
        <!-- main carousel bottom -->
        <section class="carbottom">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-3 col-sm-6 order-sm-1 order-md-1 order-lg-1">
                        <figure class="figsliderbot ">
                            <a href="productlist-chocklates.php"><img src="img/data/chocklatessliderbottom.jpg" alt="" class="img-fluid w-100"></a>
                            <article class="chock text-center">
                                <p>Celebrate Occassion With Gift </p>
                                <h2>Chocklates</h2>
                            </article>
                        </figure>
                    </div>
                    <!-- col lg 6-->
                    <div class="col-lg-6 pr-0 pl-0 col-12 servicecar order-sm-3 order-md-3 order-lg-2 ">
                        <!-- services carousel -->
                        <div id="carouselservices" class="carousel slide homeservicescar" data-ride="carousel">
                                <ol class="carousel-indicators">
                                  <li data-target="#carouselservices" data-slide-to="0" class="active"></li>
                                  <li data-target="#carouselservices" data-slide-to="1"></li>
                                  <li data-target="#carouselservices" data-slide-to="2"></li>
                                  <li data-target="#carouselservices" data-slide-to="3"></li>
                                  <li data-target="#carouselservices" data-slide-to="4"></li>
                                </ol>
                                <div class="carousel-inner">
                                  <div class="carousel-item active">
                                        <a href="onlinetutor.php"><img class="d-block w-100 img-fluid" src="img/data/servicecarousel01.jpg" alt="" title=""></a>
                                        <div class="carousel-caption">
                                            <h3><span>Find Tutors</span> in your city</h3>                                               
                                        </div>
                                  </div>
                                  <div class="carousel-item">
                                        <a href="medicalservices.php"><img class="d-block w-100 img-fluid" src="img/data/servicecarousel02.jpg" alt="" title=""></a>
                                        <div class="carousel-caption">
                                            <h3><span>Get Medical </span> Support at your home</h3>                                               
                                        </div>
                                  </div>
                                  <div class="carousel-item">
                                        <a href="visaservices.php"><img class="d-block w-100 img-fluid" src="img/data/servicecarousel03.jpg" alt="" title=""></a>
                                        <div class="carousel-caption">
                                            <h3><span>Visa Services</span> at your Doorstep</h3> 
                                        </div>
                                  </div>
                                  <div class="carousel-item">
                                        <a href="propertymanagement.php"><img class="d-block w-100 img-fluid" src="img/data/servicecarousel04.jpg" alt="" title=""></a>
                                        <div class="carousel-caption">
                                            <h3><span>Property</span> Management </h3> 
                                        </div>
                                  </div>
                                  <div class="carousel-item">
                                        <a href="visitorsinsurance.php"><img class="d-block w-100 img-fluid" src="img/data/servicecarousel05.jpg" alt="" title=""></a>
                                        <div class="carousel-caption">
                                            <h3><span>Insurance </span> Service </h3> 
                                        </div>
                                  </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselservices" role="button" data-slide="prev">
                                    <span class="icon-left-arrow-key"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselservices" role="button" data-slide="next">
                                    <span class="icon-keyboard-right-arrow-button"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>
                        <!--/ services carousel -->
                    </div>
                    <!--/ col lg 6-->
                    <div class="col-lg-3 col-sm-6 sliderrt order-sm-2 order-md-2 order-lg-3">
                        <figure class="figsliderbot ">
                            <a href="productlist.php"><img src="img/data/boutiquesliderbottom.jpg" alt="" title="" class="img-fluid"></a>
                            <article class="bou text-center">
                                <p>Visit and gift your Dear  </p>
                                <h2>Flowers</h2>
                            </article>
                        </figure>
                    </div>
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ main carousel bottom-->
        <!-- popular categories -->
        <section class="popularcat">
            <div class="container">
                <!-- title row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="hometitle">
                            <h3 class="px20 py20">Popular Categories</h3>
                        </article>
                    </div>
                </div>
                <!--/ title row -->
                <!-- caterory title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Cakes & Accessories</h4>
                            <a href="productlist-cakes.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                <!-- products row -->
                <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 text-center col-6">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/cakes/cake01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 text-center col-6">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/cakes/cake02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 text-center col-6 ">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/cakes/cake03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 text-center col-6 ">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/cakes/cake04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ products row -->
                 <!-- caterory title row -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Flowers & Bouquet</h4>
                            <a href="productlist.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                <!-- products Flowers & Boutique row -->
                <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 col-6  text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/flowers/flower01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-flower.php">Flower Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 col-6  text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/flowers/flower02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-flower.php">Flower Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6  text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/flowers/flower03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-flower.php">Flower Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/flowers/flower04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-flower.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-flower.php">Flower Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ products Flowers & Boutique row -->
                
                 <!-- caterory title row -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Gift Articles </h4>
                            <a href="productlist-gifts.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                 <!-- Gift Articles row  -->
                 <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/gifts/gift01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-gift.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-gift.php">Gift Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/gifts/gift02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-gift.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-gift.php">Gift Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/gifts/gift03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-gift.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-gift.php">Gift Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/gifts/gift04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-gift.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-gift.php">Gift Name will be here</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/Gift Articles row  -->

                 <!-- caterory title row -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Chocklates </h4>
                            <a href="productlist-chocklates.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                 <!-- Chocklates row  -->
                 <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/chocklates/chock01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-chocklates.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-chocklates.php">Chocklate Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/chocklates/chock02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-chocklates.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-chocklates.php">Chocklate Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/chocklates/chock03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-chocklates.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-chocklates.php">Chocklate Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/chocklates/chock04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-chocklates.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-chocklates.php">Chocklate Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/Chocklates row  -->

                
                 <!-- caterory title row -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Jewellery </h4>
                            <a href="productlist-jewellery.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                 <!-- Jewellery row  -->
                 <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/jewellery/jewel01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-jewellery.php">Jewellery Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/jewellery/jewel02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-jewellery.php">Jewellery Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/jewellery/jewel03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-jewellery.php">Jewellery Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/jewellery/jewel04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail-jewellery.php">Jewellery Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/Jewellery row  -->

                 <!-- caterory title row -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="titlecat">
                            <h4 class="float-left pl-4">Millets </h4>
                            <a href="productlist-millets.php" class="float-right my-1 mr-2">View All</a>
                        </div>
                    </div>
                </div>
                <!--/ caterory title row -->
                 <!-- Jewellery row  -->
                 <div class="row pb-4">
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/millets/millets01.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Millets Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                     <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/millets/millets02.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Millets Name</a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/millets/millets03.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Millets Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3 col-6 text-center">
                        <div class="productitem">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/millets/millets04.jpg" alt="" title="" class="img-fluid"></a>
                                <div class="hover">
                                    <ul class="nav">
                                        <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                        <li><a href="productdetail.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                    </ul>
                                </div>
                            </figure>
                            <article>
                                <a class="proname" href="productdetail.php">Millets Name </a>
                                <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/Jewellery row  -->


            </div>
        </section>
        <!--/ popular categories -->

        <!-- single product banner -->
        <section class="singleproductbanner" style="background-image: url('img/singlebanner.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <article>
                            <p>Celebrates  this festival SEASON with</p>
                            <h3>Chocklates</h3>
                            <a href="productlist.php">View All</a>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <!--/ single product banner -->

        <!-- Sale products tab -->
        <?php include 'homesaleproducts.php' ?>
        <!--/ sale products tab-->
        <!-- our services -->
        <section class="homeservices">
            <div class="container">
                <!-- title row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="hometitle">
                            <h3 class="px20 py20">Our Services</h3>                            
                        </article>
                        <p style="margin-top:-25px;">All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                    </div>
                </div>
                <!--/ title row -->
                <!-- row -->
                <div class="row py-4 mpb-0 tpb-0">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 pr-0 dmnone dtnone">
                                <img src="img/homemedicalimg.jpg" alt="" title="" class="img-fluid thumbimg">
                            </div>
                            <div class="col-lg-6 pl-0 mpx-15 tpl15">
                                <div class="linkcol med">
                                     <img src="img/medimg.png" alt="" title="" class="linkcolthumb">
                                     <article>
                                         <h5><a href="medicalservices.php">Medical Services</a></h5>
                                         <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                                     </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-lg-3">
                        <div class="linkcol property">
                            <img src="img/propertythumb.png" alt="" title="" class="linkcolthumb">
                            <article>
                                <h5><a href="propertymanagement.php">Property Management</a></h5>
                                <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3">
                        <div class="linkcol visa">
                            <img src="img/visathumb.png" alt="" title="" class="linkcolthumb">
                            <article>
                                <h5><a href="visaservices.php">Visa Service</a></h5>
                                <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pb-5">
                     <!-- col -->
                     <div class="col-lg-3">
                        <div class="linkcol radio">
                            <img src="img/onlineradiothumb.png" alt="" title="" class="linkcolthumb">
                            <article>
                                <h5><a href="onlineradio.php">Online Radio</a></h5>
                                <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 pr-0 dmnone dtnone">
                                <img  src="img/onlinetutorimg.png" alt="" title="" class="img-fluid thumbimg">
                            </div>
                            <div class="col-lg-6 pl-0 mpx-15 tpl15">
                                <div class="linkcol tutor">
                                    <img src="img/onlinetutorthumb.png" alt="" title="" class="linkcolthumb">
                                    <article>
                                        <h5><a href="onlinetutor.php">Online Tutor</a></h5>
                                        <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-3">
                        <div class="linkcol insurance">
                            <img src="img/visitorsthumb.png" alt="" title="" class="linkcolthumb">
                            <article>
                                <h5><a href="visitorsinsurance">Visitors Insurance</a></h5>
                                <p>Various versions have evolved over the years, sometimes by accident, sometimes.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ our services -->

        <!--terms -->
        <section class="hometerms py-4">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-4 text-center termscol col-sm-4">
                        <img src="img/hometerms01.png" alt="" title="">
                        <h5>100 % SECURE PAYMENTS</h5>
                        <p>Moving your card details to a much more secure places</p>
                    </div>
                    <div class="col-lg-4 col-sm-4 text-center termscol">
                        <img src="img/hometerms02.png" alt="" title="">
                        <h5>Trust Pay</h5>
                        <p>100% Payment Protection. Easy Return policy</p>
                    </div>
                    <div class="col-lg-4 col-sm-4 text-center termscol">
                        <img src="img/hometerms03.png" alt="" title="">
                        <h5>24/7 HELP CENTER</h5>
                        <p>Got a question? Look no further. Browse our FAQs or submit your query here.</p>
                    </div>
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ terms -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>