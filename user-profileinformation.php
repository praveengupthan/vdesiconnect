<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Profile Information</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Profile Information</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li>                              
                                <li><a>Profile Information </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                                <div class="row">
                                    <div class="col-lg-3 col-6 pic">
                                        <img  src="img/data/profiledummypic.jpg" class="img-fluid profilepic w-100">
                                        <a href="javascript:void(0)" class="new_Btn"><span class="icon-photo-camera icomoon"></span> Upload Photo</a>
                                        <input id="html_btn" type='file'" />
                                    </div>
                                    <div class="col-lg-7">
                                        <h5 class="sectitle fbold pb-3">Profile Information</h5>
                                        <div class="form-group">
                                            <label>Write Your Name</label>
                                            <input type="text" class="form-control" placeholder="Name will be here">
                                        </div>
                                        <div class="form-group">
                                            <label class="w-100">Gender Male / Female</label>
                                            <span><input type="radio"><span class="pl-1">Male</span></span>
                                            <span class="pl-3"><input type="radio"><span class="pl-1">Fe Male</span></span>
                                        </div>
                                        <div class="form-group" style="overflow:hidden;">
                                            <label class="w-100">Date of Birth</label>
                                            <input type="text" class="form-control w-25 float-left text-center" placeholder="DD">
                                            <input type="text" class="form-control w-25 float-left mx-2 text-center" placeholder="MM">
                                            <input type="text" class="form-control w-25 float-left text-center" placeholder="YYYY">
                                        </div>
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" class="form-control" placeholder="Enter Your Phone Number here">
                                        </div>
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="text" class="form-control" placeholder="Enter Your Email">
                                        </div>
                                        <div class="form-group">
                                            <label>Address Line 1</label>
                                            <input type="text" class="form-control" placeholder="Ex: H.No / Street Number / Street Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Address Line 2</label>
                                            <input type="text" class="form-control" placeholder="Location name will be here">
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label></label>
                                            <select class="form-control">
                                                <option>India</option>
                                                <option>United States of America</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>State</label></label>
                                            <select class="form-control">
                                                <option>Telangana</option>
                                                <option>Andhra Pradesh</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>City</label></label>
                                            <select class="form-control">
                                                <option>Hyderabad</option>
                                                <option>Secunderabad</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pin Number</label>
                                            <input type="text" class="form-control" placeholder="Ex: 522265">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Submit" class="greenlink w-100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>