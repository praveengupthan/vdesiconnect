<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Careers at Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Careers with us</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Career </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                   <!-- row -->
                   <div class="row">
                       <div class="col-lg-6 col-md-6">
                           <img src="img/careerimg01.jpg" alt="" title="" class="img-fluid">
                           
                       </div>
                       <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight py-2">Waht is your <span class="fbold">Next Destination</span></h5>
                           <p class="text-justify">It’s an audacious, incredibly rewarding mission that our increasingly diverse team is dedicated to achieving. Airbnb is built around the idea that everyone should be able to take the perfect trip, including where they stay, what they do, and who they meet. To that end, we empower millions of people around the world to use their spaces, passions, and talents to become entrepreneurs.</p>
                           <p>Send your Updated CV to <a class="forange" href="mailto:career@vdesiconnect.com">career@vdesiconnect.com</a></p>
                       </div>
                   </div>
                   <!--/ row -->

                   <!-- row -->
                   <div class="row py-3">
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 order-lg-last order-md-last">
                           <img src="img/career02.jpg" alt="" title="" class="img-fluid">
                       </div>
                       <!--/ col -->
                       <!-- col -->
                       <div class="col-lg-6 col-md-6 align-self-center ">
                           <h5 class="sectitle flight py-2">Create a world where anyone <span class="fbold">can belong anywhere</span></h5>
                           <p class="text-justify">It’s an audacious, incredibly rewarding mission that our increasingly diverse team is dedicated to achieving. Airbnb is built around the idea that everyone should be able to take the perfect trip, including where they stay, what they do, and who they meet. To that end, we empower millions of people around the world to use their spaces, passions, and talents to become entrepreneurs. </p>
                       </div>
                       <!--/ col -->
                      
                   </div>
                   <!--/ row -->
               </div>
              <!--/ container -->
              <!-- career banner -->
              <div class="careerban dtnone">
                 <img src="img/careerbanner.jpg" alt="" title="" class="img-fluid w-100">
              </div>
              <!--/ career banner -->
              <!-- container -->
              <div class="container">
                <!-- row -->
                    <div class="row py-4 justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h5 class="sectitle flight py-2">Live your <span class="fbold">best life</span></h5>
                            <p>There’s life at work and life outside of work. We want everyone to be healthy, travel often, get time to give back, and have the financial resources and support they need.</p>
                        </div>
                    </div>
                <!--/ row -->


                <!-- row -->
                <div class="row py-4">
                     <!-- col -->
                     <div class="col-lg-4 col-md-6 text-center careercol">
                        <figure class="svgimg py-2">
                            <span class="icon-health icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Comprehensive health plans</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                     </div>
                     <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4 col-md-6 text-center careercol">
                        <figure class="svgimg py-2">
                            <span class="icon-appointment icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Paid volunteer time</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                     </div>
                     <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-4 col-md-6 text-center careercol">
                        <figure class="svgimg py-2">
                            <span class="icon-dish icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Healthy food and snacks</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                      </div>
                      <!--/ col -->              
                     <!-- col -->
                     <div class="col-lg-4 col-md-6 text-center careercol">
                        <figure class="svgimg py-2">
                            <span class="icon-baby icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Generous parental and family leave</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                     </div>
                     <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4 col-md-6 text-center careercol">
                        <figure class="svgimg py-2">
                            <span class="icon-instruction icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Learning and development</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                     </div>
                     <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-4 col-md-6 text-center careercol ">
                        <figure class="svgimg py-2">
                            <span class="icon-honeymoon icomoon"></span>
                        </figure>
                        <article>
                            <h5 class="h5">Annual travel and experiences credit</h5>
                            <p>A world where anyone can belong anywhere starts with a workplace where you feel welcome and can contribute your best work.</p>
                        </article>
                      </div>
                      <!--/ col -->
                </div>
                <!--/ row -->
              </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>