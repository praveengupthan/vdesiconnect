<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>About Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Faq's</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Faq </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-6">
                            <div class="accordion">
                                <h3 class="panel-title">How can I check my order status?</h3>
                                <div class="panel-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique ex massa, non interdum nisl hendrerit nec. Sed metus dui, vehicula blandit nisi dictum, euismod bibendum lorem. Mauris vel ligula ut ligula facilisis porttitor quis sit amet leo. Donec sapien tellus, pulvinar a bibendum eu, ultrices vel risus. Nunc fermentum justo vitae lectus molestie, nec suscipit arcu tempor.</p>
                                </div>

                                <h3 class="panel-title">How can I return/replace an item?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What are the different modes of payment available?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What is SD+?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">How do I purchase an automobile?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What is Snapdeal's Exchange offer?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What are digital products?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">How can I cancel my order?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div> 
                            </div>
                        </div>
                        <!--/ column -->

                        <!-- column -->
                        <div class="col-lg-6">
                            <div class="accordion">
                                <h3 class="panel-title">How can I cancel my order?</h3>
                                <div class="panel-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique ex massa, non interdum nisl hendrerit nec. Sed metus dui, vehicula blandit nisi dictum, euismod bibendum lorem. Mauris vel ligula ut ligula facilisis porttitor quis sit amet leo. Donec sapien tellus, pulvinar a bibendum eu, ultrices vel risus. Nunc fermentum justo vitae lectus molestie, nec suscipit arcu tempor.</p>
                                </div>

                                <h3 class="panel-title">When can I expect refund for my returned item?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">I don't remember my password. Help!</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What is Vdesiconnect Affiliate Program?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">How can I avail installation service for my item?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">What do you mean by pre-ordering an item?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">1914 translation by H. Rackham</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div>

                                <h3 class="panel-title">Where can I get some?</h3>
                                <div class="panel-content">
                                    <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                                </div> 
                            </div>
                        </div>
                        <!--/ column -->


                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row pt-4">
                        <div class="col-lg-12 text-center">
                            <div class="whitebox p-5">
                                <a href="login.php" CLASS="greenlink px-5">LOGIN</a>
                                <p class="pt-5">Login to see your order details, track order, cancel order return / replace order etc.</p>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>