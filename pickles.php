<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pickles of Amma Cheti Vantalu </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Amma Cheti Vantalu</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                            <li><a href="index.php"> Home </a></li>
                            <li><a href="javascript:void(0)"> Amma Cheti Vantalu </a></li>                               
                            <li><a>Home Pickles </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                   <!-- row -->
                   <div class="row">
                      <!-- col 9 -->
                      <div class="col-lg-9">
                            <!-- responsive table -->
                            <table class="table" id="table-container-breakpoint">
                                <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Image</th>
                                        <th>Name of Food</th>
                                        <th>Weight</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><img src="img/data/pickles/gongura-pickle.jpg" class="img-fluid" width="75"></td>
                                    <td>Gongura Pickle</td>
                                    <td>
                                        <select class="form-control">
                                            <option>1kg</option>
                                            <option>2kg</option>
                                            <option>3kg</option>
                                            <option>4kg</option>
                                            <option>5kg</option>
                                        </select>
                                    </td>
                                    <td>$ 75</td>
                                </tr> 
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><img src="img/data/pickles/mango-pickle.jpg" class="img-fluid" width="75"></td>
                                    <td>Gongura Pickle</td>
                                    <td>
                                        <select class="form-control">
                                            <option>1kg</option>
                                            <option>2kg</option>
                                            <option>3kg</option>
                                            <option>4kg</option>
                                            <option>5kg</option>
                                        </select>
                                    </td>
                                    <td>$ 75</td>
                                </tr>              
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><img src="img/data/pickles/shrimp-pickle.jpg" class="img-fluid" width="75"></td>
                                    <td>Nimma Pickle</td>
                                    <td>
                                        <select class="form-control">
                                            <option>1kg</option>
                                            <option>2kg</option>
                                            <option>3kg</option>
                                            <option>4kg</option>
                                            <option>5kg</option>
                                        </select>
                                    </td>
                                    <td>$ 75</td>
                                </tr>              
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><img src="img/data/pickles/tomato-pickle.jpg" class="img-fluid" width="75"></td>
                                    <td>Tomato Pickle</td>
                                    <td>
                                        <select class="form-control">
                                            <option>1kg</option>
                                            <option>2kg</option>
                                            <option>3kg</option>
                                            <option>4kg</option>
                                            <option>5kg</option>
                                        </select>
                                    </td>
                                    <td>$ 75</td>
                                </tr>              
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td><img src="img/data/pickles/shrimp-pickle.jpg" class="img-fluid" width="75"></td>
                                    <td>Gongura pickle</td>
                                    <td>
                                        <select class="form-control">
                                            <option>1kg</option>
                                            <option>2kg</option>
                                            <option>3kg</option>
                                            <option>4kg</option>
                                            <option>5kg</option>
                                        </select>
                                    </td>
                                    <td>$ 75</td>
                                </tr>                                             
                                </tbody>
                            </table>
                            <!--/ responsive table -->
                      </div>
                      <!--/ col 9-->
                       <!-- right price -->
                       <div class="col-lg-3 foodvalue">                      
                           <div>
                                <p>Total Weight You Selected</p>
                                <h3 class="h3"><strong>9</strong> kg</h3>
                           </div>
                           <div>
                                <p>Total Items You Selected</p>
                                <h3 class="h3"><strong>4</strong></h3>
                           </div>
                           <div>
                                <p>Total Value</p>
                                <h3 class="h3"><strong>$150</strong></h3>
                           </div>
                           <div>
                                <p>Grand Total</p>
                                <h3 class="h3"><strong class="forange">$165</strong></h3>
                           </div>
                           <div class="foodbtn">
                                <a href="javascript:void(0)" class="greenlink btn disabled">Buy Now</a>
                           </div>
                           <p class="py-3" style="color:red;">Minimum Purchase Required 10 Kg</p>
                       </div>
                       <!--/ right price -->
                   </div>
                   <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->   
</body>
</html>