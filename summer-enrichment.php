<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Medical Services at Vdesi Connect </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Summer Enrichment Services</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                            <li><a href="index.php"> Home </a></li>                               
                                <li><a> Summer Enrichment </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="img/summerinrichment01.jpg" alt="" title="" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">Summer Enrichment  <span class="fbold">Programs</span></h5>
                            <p class="text-justify">Feeling Kids are staying Home and doing nothing in this Summer.  How about Summer Enrichment Programs at your convenience, Available Online, So no need to Commute.</p>
                            <p>Please contact us for details so that we can customize Based on your requirements.  Either you can pick and choose one class each day of the week or We can customize based on your Requirement. </p>
                            <p>Submit the Service Request Enquiry form and we will get back to you soon to discuss the package Details.</p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">How it <span class="fbold">Works</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                   <!-- row -->
                   <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center">
                                <h5 class="fgreen text-uppercase"><span>1</span>Login and Submit Request</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Login and Submit Request with all your Personal Details</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>2</span>Review Request</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Our team will review the request and gather the information you need</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>3</span>Contact & Quote</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Our team will contact you and discuss on your needs, find appropriate resources and pricing.</p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>4</span>Payment</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>We will send invoice for customer approval and payment.</p>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->                   

                    <div class="whitebox p-4 my-3">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                            <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span></h5>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia possimus iste, dolores ut debitis cupiditate recusandae, consectetur molestias numquam laborum nostrum quis, obcaecati aliquid. Maiores.</p>
                                <a href="login.php" class="greenlink">Login and Send Request</a>
                            </div>
                        </div>
                    </div>

                   <div class="whitebox p-4">
                         <!-- row -->
                        <div class="row">
                             <div class="col-lg-12">
                                <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your Requirement </b></h5>
                                <p>Our Exeucitve will follow following Contact details, Please Give your Genuine Contact Details</p>
                             </div>
                        </div>
                        <!--/ row -->
                        
                       <!-- service form -->
                       <form class="serviceform">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Person Name</label>
                                        <input type="text" placeholder="Enter Contact Person Name" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <input type="text" placeholder="Address Line 01" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Address Line 2</label>
                                        <input type="text" placeholder="Address Line 02" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->
                                
                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Service Required Country</label>
                                        <select class="form-control">                                            
                                            <option>India</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>State </label>
                                        <select class="form-control">
                                            <option>Select State</option>
                                            <option>Telangana</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control">
                                            <option>Hyderabad</option>
                                            <option>Secunderabad </option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Select Summer Service</label>
                                        <select class="form-control">                                            
                                            <option>Vocal</option>
                                            <option>Dance Classical (Kuchipudi or Bharatanatyam or Bollywood)</option>
                                            <option>Guitar</option>
                                            <option>Piano</option>
                                            <option>Violin</option>
                                            <option>Chess</option>
                                            <option>Drums</option>  
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="w-100">Date & Time of Service Required</label>
                                        <input class="form-control datepicker"/>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Preferred Timings to Contact</label>
                                        <input type="text" placeholder="Ex: 10 AM" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Email</label>
                                        <input type="text" placeholder="Enter Correct Email Address" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Phone</label>
                                        <input type="text" placeholder="+91 9642123254" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message </label>
                                        <textarea class="form-control" placeholder="Enter your Message"></textarea>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row pt-3">
                                <div class="col-lg-4">
                                    <input type="submit" value="Submit Request" class="greenlink">
                                </div>
                            </div>
                            <!--/ row -->
                        </form>
                        <!--/ service form -->
                   </div>
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>