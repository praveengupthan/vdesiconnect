<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Order History </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">My order History</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li>                              
                                <li><a href="user-myorders.php">Order History </a></li>
                                <li><a>Cancel / Returns </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">                               
                                <!-- tab -->
                                <div class="myorderhistory">
                                     <div class="taborderhistory">
                                        <?php include 'myordersnav.php' ?>
                                        <div class="resp-tabs-container hor_1 p-0"> 
                                              <!-- Cancel / Return -->
                                              <div>
                                                <h4 class="coltitle">Cancel / Returns</h4>
                                                 <!-- user product -->
                                                 <div class="userproduct">
                                                    <ul class="row primarydetails">
                                                        <li class="col-lg-4 col-md-6">
                                                            <h6>Name</h6>
                                                            <p>Praveen Kumar Nandipati</p>
                                                        </li>
                                                        <li class="col-lg-4 col-md-6">
                                                            <h6>Order number	</h6>
                                                            <p>18100614451880850561	</p>
                                                        </li>
                                                        <li class="col-lg-4 col-md-6">
                                                            <h6>Order Date & Time </h6>
                                                            <p>06 Oct 2018 14:45:19	</p>
                                                        </li>
                                                    </ul>
                                                    <!-- row -->
                                                    <div class="row pb-3">
                                                        <!-- col -->
                                                        <div class="col-lg-2 col-md-3">
                                                            <figure class="imgproduct">
                                                                <a href="user-myordersdetail.php"><img src="img/data/flowers/flower02.jpg" alt="" title="" class="img-fluid"></a>
                                                            </figure>
                                                        </div>
                                                        <!--/ col -->
                                                        <!-- col -->
                                                        <div class="col-lg-7 col-md-9">
                                                            <h6 class="pb-2">Product Name will be here</h6>
                                                            <p>Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                                            
                                                            <a href="user-myordersdetail.php" class="whitebtn">Order Details</a>                                                           
                                                        </div>
                                                        <!--/ col -->
                                                        <!-- col -->
                                                        <div class="col-lg-3 col-12 text-right">
                                                            <h2 class="h2">Rs: 498</h2>
                                                        </div>
                                                        <!-- col -->
                                                        <div class="col-lg-12">
                                                            <p class="pt-3">Replacement was allowed till Sat, 25 Oct 2014. </p>
                                                        </div>
                                                    </div>
                                                    <!--/ row -->
                                                 </div>
                                                 <!-- /user product -->
                                             </div>
                                             <!--/ Cancel / Return -->

                                        </div>
                                     </div>
                                </div>
                                <!--/ tab -->                         
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>