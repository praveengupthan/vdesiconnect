<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Visa Services at Vdesi Connect </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Visa Support Services</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                            <li><a href="index.php"> Home </a></li>                               
                                <li><a> Visa Support Services </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="img/visaservice02img.jpg" alt="" title="" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">We Help your any <span class="fbold">Visa and Support Services</span></h5>
                            <p class="text-justify">Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence on cross-platform integration. Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. ive on the start-up mentality to derive convergence on cross-platform.</p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">How it <span class="fbold">Works</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center">
                                <h5 class="fgreen text-uppercase"><span>1</span>Login and Submit Request</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Login and Submit Request with all your Personal Details</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>2</span>Review Request</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Our team will review the request and gather the information you need</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>3</span>Contact & Quote</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>Our team will contact you and discuss on your needs, find appropriate resources and pricing.</p>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3 col-md-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>4</span>Payment</h5>
                                <figure>
                                    <span class="icon-knowledge icomoon"></span>
                                </figure>
                                <p>We will send invoice for customer approval and payment.</p>
                            </div>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col -->
                        <div class="col-lg-6 order-lg-last col-md-6 align-self-center">
                            <img src="img/visaservice01img.jpg" alt="" title="" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">Advantages with <span class="fbold">Visa Support Services</span></h5>
                           <ul class="listcontent">
                               <li>Get all your Visa work with our reliable and experienced professional team. We will update you on the process at each stage.  </li>
                               <li>Podcasting operational change management inside of workflows to establish a framework. Taking seamless </li>
                               <li>Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. </li>
                               <li>Podcasting operational change management inside of workflows to establish a framework. Taking seamless </li>
                           </ul>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <div class="whitebox p-4 my-3">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                            <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span></h5>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia possimus iste, dolores ut debitis cupiditate recusandae, consectetur molestias numquam laborum nostrum quis, obcaecati aliquid. Maiores.</p>
                                <a href="login.php" class="greenlink">Login and Send Request</a>
                            </div>
                        </div>
                    </div>

                   <div class="whitebox p-4">
                         <!-- row -->
                        <div class="row">
                             <div class="col-lg-12">
                                <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your Requirement </b></h5>
                                <p>Our Exeucitve will follow following Contact details, Please Give your Genuine Contact Details</p>
                             </div>
                        </div>
                        <!--/ row -->
                        
                           <!-- service form -->
                           <form class="serviceform">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Person Name</label>
                                        <input type="text" placeholder="Enter Contact Person Name" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <input type="text" placeholder="Address Line 01" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Address Line 2</label>
                                        <input type="text" placeholder="Address Line 02" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->
                                
                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Service Required Country</label>
                                        <select class="form-control">                                            
                                            <option>India</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>State </label>
                                        <select class="form-control">
                                            <option>Select State</option>
                                            <option>Telangana</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control">
                                            <option>Hyderabad</option>
                                            <option>Secunderabad </option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Select Tutor</label>
                                        <select class="form-control">                                            
                                            <option>Student Visas (Eg: UK, USA, Australia, Canada etc.,)</option>
                                            <option>Work Visas (Eg: USA, UAE, UK, Australia etc.,)</option>
                                            <option>Vistor Visas</option>
                                            <option>Other Visas</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="w-100">Date & Time of Service Required</label>
                                        <input class="form-control datepicker"/>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Preferred Timings to Contact</label>
                                        <input type="text" placeholder="Ex: 10 AM" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Email</label>
                                        <input type="text" placeholder="Enter Correct Email Address" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Contact Phone</label>
                                        <input type="text" placeholder="+91 9642123254" class="form-control">
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message </label>
                                        <textarea class="form-control" placeholder="Enter your Message"></textarea>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row pt-3">
                                <div class="col-lg-4">
                                    <input type="submit" value="Submit Request" class="greenlink">
                                </div>
                            </div>
                            <!--/ row -->
                        </form>
                        <!--/ service form -->
                   </div>

                   


               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>