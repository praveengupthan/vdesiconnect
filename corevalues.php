<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Core Values of Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Core Values</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Core Values </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h5 class="sectitle fbold py-3 text-uppercase fgreen">CONNECTING THE INSIDE OUT...</h5>
                            <p>We have always taken pride in our culture. There are some core values that have been inherent and are an integral part of our success story. These values are a pure reflection of what is important to us as a Team and Business.</p>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row whitebox p-2 pb-0">
                        <div class="col-lg-6 pl-0 col-sm-12">
                            <img src="img/corevalues01.jpg" alt="" title="" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 col-sm-12 align-self-center">
                            <h5 class="sectitle py-3">Be <span class="fbold">Relentless</span></h5>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 pl-0 order-lg-last">
                            <img src="img/corevalues02.jpg" alt="" title="" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <h5 class="sectitle py-3">Own <span class="fbold"> Outcomes </span></h5>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 pl-0">
                            <img src="img/corevalues03.jpg" alt="" title="" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <h5 class="sectitle py-3">Always Be <span class="fbold">Curious</span></h5>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                        </div>
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 pl-0 order-lg-last">
                            <img src="img/corevalues04.jpg" alt="" title="" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <h5 class="sectitle py-3">Don’t <span class="fbold">disrespect</span></h5>
                            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,  </p>
                        </div>
                    </div>
                    <!--/ row -->


               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>