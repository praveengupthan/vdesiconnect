<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Occassions Reminders </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Occassions Reminders</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li>                              
                                <li><a>Occassions Reminders</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">Occassions Reminders </h5>
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="whitebox occasion p-3">
                                            <h5 class="fbold">Wedding Day <span class="float-right forange">27 July 2018</span></h5>
                                            <p class="py-3">Bhuvana and Praveen Wedding Anniversay who are my class mates from my child hood friends and they are very best friends</p>
                                            <a class="greenlink" href="productlist.php">Send Gift</a> <a class="whitebtn" href="javascript:void(0)">X Remove from List</a> 
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="whitebox occasion p-3">
                                            <h5 class="fbold">Birthday <span class="float-right forange">27 July 2018</span></h5>
                                            <p class="py-3">Bhuvana and Praveen Wedding Anniversay who are my class mates from my child hood friends and they are very best friends</p>
                                            <a class="greenlink" href="productlist.php">Send Gift</a> <a class="whitebtn" href="javascript:void(0)">X Remove from List</a> 
                                        </div>
                                    </div>
                                    <!--/ col -->                                
                                </div>
                                <!--/ row -->
                                <!-- row -->
                                <div class="row pt-4">
                                    <div class="col-lg-12">
                                        <h5 class="fbold h5">Create a Reminder (Note: All fields are Mandatory)</h5>

                                        <!-- form -->
                                        <form>
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Name of Reminder</label>
                                                        <input type="text" placeholder="Ex: Ravi Wedding Anniversary" class="form-control">
                                                    </div>
                                                </div>
                                                <!-- col -->

                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Select Occassion</label>
                                                        <select class="form-control">
                                                            <option>Select Occassion </option>
                                                            <option>Wedding Anniversary </option>
                                                            <option>Birthday </option>
                                                            <option>Other Occassion </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- col -->
                                            </div>
                                            <!--/ row -->

                                             <!-- row -->
                                             <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="w-100">Date of Occassion</label>
                                                        <input type="text" class="form-control w-25 float-left text-center" placeholder="DD">
                                                        <input type="text" class="form-control w-25 float-left mx-2 text-center" placeholder="MM">
                                                        <input type="text" class="form-control w-25 float-left text-center" placeholder="YYYY">
                                                    </div>
                                                </div>
                                                <!-- col -->

                                                <!-- col -->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Remind </label>
                                                        <select class="form-control">
                                                            <option>1 Day Before </option>
                                                            <option>2 Days Before </option>
                                                            <option>3 Days Before </option>
                                                            <option>4 Days Before </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- col -->
                                            </div>
                                            <!--/ row -->

                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Reminding Subject</label>
                                                        <textarea class="form-control" placeholder="Write About Your Occassion" style="height:120px;"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <input type="submit" value="Set Reminder" class="greenlink">
                                                    <input type="submit" value="Reset" class="greenlink">
                                                </div>
                                            </div>
                                            <!--/ row -->


                                        </form>
                                        <!--/ form -->
                                    </div>
                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts --> 
</body>
</html>