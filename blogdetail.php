<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Blog Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Blog</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a href="bloglist.php">Blog </a></li>
                                <li><a>Blog Detail Name will be here </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- col-lg-8-->
                        <div class="col-lg-8 blogdetail">
                            <figure>
                                <img src="img/data/blog/blogimg01.jpg" alt="" title="" class="img-fluid w-100">
                            </figure>
                            <article>
                                <h3>Vdesiconnect empowered me to be the businesswoman I am today!</h3>
                                <p class="det"><span>7 May 2018 </span><span>By Admin </span></p>
                                <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <p class="text-justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                                <p class="text-justify">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, Ipsum passage, and going through the cites of the word in classical literature,</p>
                            </article>
                        </div>
                        <!--/ col-lg-8 -->

                        <!-- col lg 4-->
                        <div class="col-lg-4 recentnews">
                            <h3 class="h3 border-none">Recent News & Updates</h3>
                            <!-- row -->
                            <div class="row">
                               
                                <!-- col -->
                                <div class="col-lg-12 col-sm-6">
                                    <div class="recentcol">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/blog/blogimg02.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="position-absolute">7 May 2018</span>
                                        </figure>
                                        <article>
                                            <a href="javascript:void(0)">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12 col-sm-6">
                                    <div class="recentcol">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/blog/blogimg01.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="position-absolute">7 May 2018</span>
                                        </figure>
                                        <article>
                                            <a href="javascript:void(0)">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12 col-sm-6">
                                    <div class="recentcol">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/blog/blogimg03.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="position-absolute">7 May 2018</span>
                                        </figure>
                                        <article>
                                            <a href="javascript:void(0)">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-12 col-sm-6">
                                    <div class="recentcol">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="img/data/blog/blogimg04.jpg" alt="" title="" class="img-fluid"></a>
                                            <span class="position-absolute">7 May 2018</span>
                                        </figure>
                                        <article>
                                            <a href="javascript:void(0)">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                        </article>
                                    </div>
                                </div>
                                <!--/ col -->

                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ col lg 4 -->
                    </div>
                    <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>