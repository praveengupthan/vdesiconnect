<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Jewellery</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="javascript:void(0)">category</a></li>
                                <li><a>Jewellery</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
               <!-- filter container -->
               <div class="container">
                   <!-- row -->
                   <div class="row filterrow">
                        <div class="col-lg-6 col-6">
                            <h5>Jewellery  ( 24 Items )</h5>
                        </div>
                        <div class="col-lg-6 col-6 text-right sortcol">                            
                            <div class="dropdown">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                Sortby
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Price Low to High</a>
                                    <a class="dropdown-item" href="#">Price High to Low</a>
                                    <a class="dropdown-item" href="#">Popular Products</a>
                                </div>
                            </div>
                        </div>
                   </div>
                   <!--/ row -->                   
               </div>
               <!--/ filter container -->
               <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                       <!-- col -->
                       <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel01.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel02.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel03.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                       <!-- col -->
                       <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel04.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel05.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel06.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel07.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel08.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel09.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel10.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel11.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel12.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel13.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel14.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel15.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel16.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel17.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel18.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel19.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel20.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel21.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel22.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel23.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-6 col-md-4 text-center">
                            <div class="productitem">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/jewellery/jewel24.jpg" alt="" title="" class="img-fluid"></a>
                                    <div class="hover">
                                        <ul class="nav">
                                            <li><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"><span class="icon-heartuser icomoon"></span></a></li>
                                            <li><a href="productdetail-jewellery.php" data-toggle="tooltip" data-placement="bottom" title="View More"><span class="icon-external-link icomoon"></span></a></li>
                                        </ul>
                                    </div>
                                </figure>
                                <article>
                                    <a class="proname" href="productdetail-jewellery.php">Product Name will be here</a>
                                    <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                    <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                </article>
                            </div>
                       </div>
                       <!--/ col -->
                   </div>
                   <!-- row -->
               </div>
               <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->    
</body>
</html>