<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Wishlist Products </title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">My Wishlist</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li> 
                                <li><a href="user-profileinformation.php">Praveen Kumar Nandipati </a></li> 
                                <li><a>My Wishlist Products</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                   <!-- row -->
                   <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav col-md-4">
                           <?php include 'userleftnav.php' ?>
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">My Wishlist </h5>
                                <!-- row -->
                                <div class="row">
                                <!-- small column -->
                                <div class="col-lg-12">
                                    <div class="smallcol mb-2 position-relative">
                                    <span class="outofstock"><img src="img/sold-outimg.png" alt="" title=""></span>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-md-9 pl-0">                                               
                                                <article class="pt-2">
                                                    <h5><a href="productdetail.php">Gift Article Item Name will be here </a></h5>
                                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document. </p>
                                                </article>                                                
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <span class="oldprice">Rs:5000</span><span class="price">Rs: 3,200</span> <span class="price">(-25%)</span>
                                                    </div>                                                    
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-online-shopping-cart icomoon"></span> Add to Cart</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span> Remove from Wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                 <!-- small column -->
                                 <div class="col-lg-12">
                                    <div class="smallcol mb-2 position-relative">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-md-9 pl-0">                                               
                                                <article>
                                                    <h5><a href="productdetail.php">Gift Article Item Name will be here </a></h5>
                                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document. </p>
                                                </article>                                                
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <span class="oldprice">Rs:5000</span><span class="price">Rs: 3,200</span> <span class="price">(-25%)</span>
                                                    </div>                                                    
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-online-shopping-cart icomoon"></span> Add to Cart</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span> Remove from Wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                 <!-- small column -->
                                 <div class="col-lg-12">
                                    <div class="smallcol mb-2 position-relative">
                                    <span class="outofstock"><img src="img/sold-outimg.png" alt="" title=""></span>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3">
                                                <figure>
                                                    <a href="productdetail.php"><img src="img/data/cakes/cake04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                                </figure>
                                            </div>
                                            <div class="col-lg-9 col-md-9 pl-0">                                               
                                                <article>
                                                    <h5><a href="productdetail.php">Gift Article Item Name will be here </a></h5>
                                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document. </p>
                                                </article>                                                
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <span class="oldprice">Rs:5000</span><span class="price">Rs: 3,200</span> <span class="price">(-25%)</span>
                                                    </div>                                                    
                                                </div>
                                                <ul class="addstosmallcol nav">
                                                    <li><a href="javascript:void(0)"><span class="icon-online-shopping-cart icomoon"></span> Add to Cart</a></li>
                                                    <li><a href="javascript:void(0)"><span class="icon-bin icomoon"></span> Remove from Wishlist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- small column -->

                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                   </div>
                   <!--/ row -->                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts --> 
</body>
</html>