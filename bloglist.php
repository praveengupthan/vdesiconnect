<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Blog Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Blog</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>                               
                                <li><a>Blog </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                    <!-- row -->
                    <div class="row">

                        <!-- col -->
                        <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg01.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg02.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg03.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg04.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg01.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6 blogcol col-sm-6">
                            <figure><a href="blogdetail.php"><img src="img/data/blog/blogimg02.jpg" alt="" title="" class="img-fluid"></a></figure>
                            <article class="text-center">
                                <a href="blogdetail.php">Vdesiconnect empowered me to be the businesswoman I am today!</a>
                                <p><span>7 May 2018 </span><span>By Admin </span></p>
                                <a href="blogdetail.php" class="linkblog">READ MORE</a>
                            </article>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->
                    
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>