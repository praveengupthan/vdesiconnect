<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Payment Success</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Payment Success</h1>                            
                            </article>                           
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <!-- successful article -->
                        <div class="col-lg-12 text-center successpayment py-4 border-bottom">
                            <p><span class="icon-tick-inside-circle icomoon"></span></p>
                            <h5 class="sectitle fbold pt-1 mb-0 pb-0"> <h5 class="sectitle  py-3"><span class="fbold fgreen">Payment Success</span></h5></h5>
                            <p>Thank you! Your Payment of Rs: 3,000 has been received</p>
                            <p><span>Order ID: IC-12334567</span><span class="px-4">Transaction ID: 123456</span></p>
                            <p clas="fbold">Click on the following Button, You Can Track the Order Status, Payment Details of Product  from Order Booking to Delivery at your doorstep</p>
                            <a href="user-myorders.php"class="whitebtn">Track Order</a>
                            <a href="javascript:void(0)"class="greenlink">View Invoice</a>
                            <a href="index.php"class="whitebtn">Continue Shopping</a>
                        </div>
                        <!--/ successful article -->
                        <!--/left col -->                       
                    </div>
                    <!--/ row -->
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>