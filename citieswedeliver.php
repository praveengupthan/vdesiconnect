<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>About Vdesi Connect</title>
    <?php include 'headerstyles.php' ?>
</head>
<body>
    <!--header -->
    <?php include 'header.php' ?>
    <!--/ header-->
    <!--main -->
    <main>
       <!-- sub apge -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpageheader">
               <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
               <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
               <div class="container">
                   <div class="row justify-content-center">
                       <div class="col-lg-8 text-center">
                             <article class="pagetitle">
                                <h1 class="px20 py20">Cities We Deliver</h1>                            
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                            <li><a href="index.php"> Home </a></li>                               
                                <li><a> Cities we Deliver </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                       </div>
                   </div>
               </div>
           </section>
           <!--/ sub page header -->
           <!--sub page main -->
           <section class="subpagemain">
              <!-- container -->
               <div class="container stpage">
                  <!-- row -->
                  <div class="row justify-content-center pb-3">

                       <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/cityhydimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Hyderabad</h4>
                                            <p><small class="fgreen">Telangana State, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/cityamaravatiimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Amaravati</h4>
                                            <p><small class="fgreen">Andhra Pradesh, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                  </div>
                  <!--/ row -->  

                   <!-- row -->
                   <div class="row justify-content-center pb-3">
                       <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/chennaicityimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Chennai City</h4>
                                            <p><small class="fgreen">Tamilnadu State, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/bengalurucityimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Bengaluru City</h4>
                                            <p><small class="fgreen">Karnataka, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                  </div>
                  <!--/ row -->  

                  <!-- row -->
                  <div class="row justify-content-center pb-3">
                       <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/mumbaicityimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Mumbai City</h4>
                                            <p><small class="fgreen">Maharastra State, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="img/thiruvanananthapuramcityimg.jpg" class="img-fluid h-100 w-100">
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Thiruvananthapuram</h4>
                                            <p><small class="fgreen">Kerala, India</small></p>
                                            <p class="text-justify">Hyderabad City Tour Short Family TripKirty Holiday Charminar Hyderabad Stock Footage 4K and HD Clips Hyderabad is 2nd Best Place in the World charminar pos Browse Charminar Hyderabad </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                  </div>
               </div>
              <!--/ container -->
           </section>
           <!--/ sub page main -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    <!-- footer scripts -->
    <?php include 'footerscripts.php' ?>
    <!--/ footer scripts -->
</body>
</html>